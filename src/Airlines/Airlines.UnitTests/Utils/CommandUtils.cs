﻿using Airlines.Business.Route;
using Airlines.Business.Commands;
using Airlines.Business.Commands.Batch;
using Airlines.Persistence.Basic.Container;

namespace Airlines.UnitTests.Utils
{
    public class CommandUtils
    {
        public static ICommand ExecuteCommand(RepositoryContainer repositoryContainer,
            RouteGraph route,
            BatchData batchData,
            params string[] inputParts)
        {
            var command = CommandFactory.Create(inputParts, repositoryContainer, route, batchData);

            command.Execute();

            return command;
        }
    }
}
