﻿using System.Text;

using Airlines.DataContracts.Enums;

namespace Airlines.UnitTests.Utils
{
    public static class SortTestingUtils
    {
        public static bool IsSortedAscending(this string[] array)
        {
            for (var i = 1; i < array.Length; i++)
            {
                if (array[i - 1].CompareTo(array[i]) == (int)ComparingTypes.Bigger)
                {
                    return false;
                }
            }

            return true;
        }

        public static string[] GenerateRandomArray(int? size = null, int randomSizeMin = 2, int randomSizeMax = 100)
        {
            var rand = new Random();
            size ??= rand.Next(randomSizeMin, randomSizeMax);

            var array = new string[size.Value];

            for (var i = 0; i < array.Length; i++)
            {
                array[i] = GenerateRandomString();
            }

            return array;
        }

        public static string GenerateRandomString(int sizeMax = 20)
        {
            var rand = new Random();
            var size = rand.Next(1, sizeMax);

            var result = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                result.Append((char)rand.Next(1, 128));
            }

            return result.ToString();
        }
    }
}
