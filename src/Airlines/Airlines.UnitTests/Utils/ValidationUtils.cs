﻿namespace Airlines.UnitTests.Utils
{
    public static class ValidationUtils
    {
        public static void TestThatWorksSuccessfully(Action func) => func();

        public static void TestThatFails(Action func)
        {
            try
            {
                func();
                throw new Exception("Test failed! The func worked successfully!");
            }
            catch
            {

            }
        }
    }
}
