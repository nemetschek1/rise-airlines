﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.RouteFlights;

namespace Airlines.UnitTests.Utils
{
    public class RouteTestsUtils
    {
        public static bool IsFlightWithIdentifierAddedToRoute(string flightIdentifier,
            IRouteFlightRepository<Flight> repositoryContainer)
        {
            try
            {
                var flight = repositoryContainer.FindByKey(flightIdentifier);
                ArgumentNullException.ThrowIfNull(flight);

                return flight.DepartureAirport!.FlightsFromCurrent.Contains(flight);
            }
            catch
            {
                return false;
            }
        }
    }
}