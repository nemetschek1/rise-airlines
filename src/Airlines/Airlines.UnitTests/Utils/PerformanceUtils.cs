﻿using Airlines.Persistence.Basic.Algorithms.Sort;
using System.Diagnostics;

namespace Airlines.UnitTests.Utils
{
    public class PerformanceUtils
    {
        public static int CountIterationsOfFunctionForGivenPeriodOfTime(double timeInSeconds, Action<Stopwatch> function)
        {
            var iterations = 0;
            var stopwatch = Stopwatch.StartNew();
            while (stopwatch.Elapsed.TotalSeconds < timeInSeconds)
            {
                function(stopwatch);
                iterations++;
            }

            return iterations;
        }

        public static void UseSortOnNewArray(Stopwatch sw, string[] array, SortAlgorithm sorting)
        {
            sw.Stop();

            var copyArray = (string[])array.Clone();

            sw.Start();

            sorting.Execute(copyArray);
        }

        public static int GetRandomIndexInArray(int length)
        {
            var random = new Random();

            return random.Next(length);
        }
    }
}
