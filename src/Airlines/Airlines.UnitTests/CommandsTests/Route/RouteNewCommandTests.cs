﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteNewCommandTests
    {
        [Fact]
        public void Execute_SetsEmptyInstanceOfRoute_Successfully()
        {
            var startAirportName = "JFK";

            var inputParams = new string[] { "route", "new", startAirportName };

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "read", "airports", "./Resources/airports.csv");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, inputParams);

            Assert.True(route.Count == 0);
        }
    }
}