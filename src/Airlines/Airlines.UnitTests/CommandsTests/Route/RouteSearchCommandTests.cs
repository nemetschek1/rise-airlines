﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteSearchCommandTests
    {
        [Theory]
        [InlineData("stops", "ATL", "BOS")]
        [InlineData("short", "ATL", "BOS")]
        [InlineData("cheap", "ATL", "BOS")]
        public void Execute_DirectFlight_Successfully(
            string strategy, string startAirportName, string destinationAirportName)
        {
            var expectedOutput = $"Shortest route to airport:{Environment.NewLine}{startAirportName} -> {destinationAirportName}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "search", startAirportName, "->", destinationAirportName, strategy);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }

        [Theory]
        [InlineData(["stops", "ATL", "DFW", new string[] { "BOS", "SEA" }])]
        [InlineData(["short", "ATL", "DFW", new string[] { "BOS", "SEA" }])]
        [InlineData(["cheap", "ATL", "DFW", new string[] { "BOS", "SEA" }])]
        public void Execute_IndirectFlight_Successfully(
            string strategy, string startAirportName, string destinationAirportName, string[] stopsInTheMiddle)
        {
            var expectedOutput = $"Shortest route to airport:{Environment.NewLine}" +
                $"{startAirportName} -> {string.Join(" -> ", stopsInTheMiddle)} -> {destinationAirportName}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "search", startAirportName, "->", destinationAirportName, strategy);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }

        [Theory]
        [InlineData("stops")]
        [InlineData("short")]
        [InlineData("cheap")]
        public void Execute_NoRoute_Successfully(string strategy)
        {
            var startAirportName = "ATL";
            var searchedAirportName = "DCA";

            var expectedOutput = $"No route to airport exists";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "search", startAirportName, "->", searchedAirportName, strategy);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }
    }
}
