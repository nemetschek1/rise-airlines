﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteFindCommandTests
    {
        [Fact]
        public void Execute_WithNoExistingAirport_Successfully()
        {
            var expectedOutput = "Routes to searched airport:";

            var startAirportName = "JFK7";
            var searchedAirportName = "MEN ME NQMA";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var batchData = new BatchData();

            var findCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "find", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, findCommand.Output?.Trim());
        }

        [Fact]
        public void Execute_WithExistingAirport_DirectOneFlight_Successfully()
        {
            var startAirportName = "DFW";
            var searchedAirportName = "JFK";

            var expectedOutput = $"Routes to searched airport:{Environment.NewLine}{startAirportName} -> {searchedAirportName}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var batchData = new BatchData();

            var findCommand = CommandUtils.ExecuteCommand(
                 repositoryContainer, route, batchData, "route", "find", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, findCommand.Output?.Trim());
        }

        [Fact]
        public void Execute_WithExistingAirport_InirectOneFlight_Successfully()
        {
            var startAirportName = "DFW";
            var searchedAirportName = "LAX";
            var expectedOutput = $"Routes to searched airport:{Environment.NewLine}{startAirportName} -> JFK -> {searchedAirportName}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var batchData = new BatchData();

            var findCommand = CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "find", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, findCommand.Output?.Trim());
        }
    }
}
