﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteAddCommandTests
    {
        [Fact]
        public void Execute_AddsNewFlightToRoute_ThatDoesExist_Successfully()
        {
            var addedFlightIdentifier = "FL505";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "new");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", addedFlightIdentifier);

            var isAdded = RouteTestsUtils.IsFlightWithIdentifierAddedToRoute(addedFlightIdentifier, repositoryContainer.RouteFlights);

            Assert.True(isAdded);
        }

        [Fact]
        public void Execute_AddsNewFlightToRoute_ThatDoesNotExist_ThrowsException()
        {
            var searchedFlightIdentifier = "asdasd";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "new");

            var func = () => CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", searchedFlightIdentifier);

            Assert.Throws<ArgumentNullException>(func);
        }
    }
}