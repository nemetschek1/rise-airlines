﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteCheckCommandTests
    {
        [Fact]
        public void Execute_RouteExists_DirectFlight()
        {
            var expectedOutput = "Route exists";

            var startAirportName = "JFK";
            var searchedAirportName = "LAX";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "check", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }

        [Fact]
        public void Execute_RouteExists_IndirectFlight()
        {
            var expectedOutput = "Route exists";

            var startAirportName = "DFW";
            var searchedAirportName = "LAX";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "check", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }

        [Fact]
        public void Execute_RouteDoesNotExist()
        {
            var expectedOutput = "Route does NOT exist";

            var startAirportName = "JFK7";
            var searchedAirportName = "MEN ME NQMA";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var checkCommand = CommandUtils.ExecuteCommand(
                repositoryContainer, route, batchData, "route", "check", startAirportName, "->", searchedAirportName);

            Assert.Equal(expectedOutput, checkCommand.Output?.Trim());
        }
    }
}
