﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RoutePrintCommandTests
    {
        [Fact]
        public void Execute_ShowsTwoRoutesSuccessfully()
        {
            var expectedOutput = $"DCA -> SFO{Environment.NewLine}SFO -> MIA";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "new");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", "FL101");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", "FL202");

            var printCommand = CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "print");

            Assert.Equal(expectedOutput, printCommand.Output?.Trim());
        }
    }
}