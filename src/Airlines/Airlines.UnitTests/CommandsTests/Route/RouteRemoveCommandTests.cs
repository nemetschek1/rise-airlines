﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Route
{
    public class RouteRemoveCommandTests
    {
        [Fact]
        public void Execute_WithOneFlight_Successful()
        {
            var flightIdentifier = "FL456";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "new", "LAX");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", flightIdentifier);

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "remove", flightIdentifier);

            Assert.False(RouteTestsUtils.IsFlightWithIdentifierAddedToRoute(flightIdentifier, repositoryContainer.RouteFlights));
        }

        [Fact]
        public void Execute_WithTwoFlight_Successful()
        {
            var containedFlightIdentifier = "FL123";
            var removedFlightIdentifier = "FL456";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "new", "JFK");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", containedFlightIdentifier);
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "add", removedFlightIdentifier);

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "route", "remove", removedFlightIdentifier);

            Assert.True(RouteTestsUtils.IsFlightWithIdentifierAddedToRoute(containedFlightIdentifier, repositoryContainer.RouteFlights));
            Assert.False(RouteTestsUtils.IsFlightWithIdentifierAddedToRoute(removedFlightIdentifier, repositoryContainer.RouteFlights));
        }
    }
}