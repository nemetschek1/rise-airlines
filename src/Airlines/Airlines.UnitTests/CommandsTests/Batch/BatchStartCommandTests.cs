﻿using Airlines.Business.Commands.Batch;

namespace Airlines.UnitTests.CommandsTests.Batch
{
    public class BatchStartCommandTests
    {
        [Fact]
        public void Execute_Successfully()
        {
            var batchData = new BatchData();

            var batchStartCommand = BatchStartCommand.Create(batchData);

            batchStartCommand.Execute();

            Assert.True(batchData.IsBatchModeActive);
        }
    }
}
