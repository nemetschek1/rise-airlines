﻿using Airlines.Business.Commands.Batch;

namespace Airlines.UnitTests.CommandsTests.Batch
{
    public class BatchCancelCommandTests
    {
        [Fact]
        public void Execute_Successfully()
        {
            var batchData = new BatchData();

            var batchStartCommand = BatchCancelCommand.Create(batchData);

            batchStartCommand.Execute();

            Assert.False(batchData.IsBatchModeActive);
        }
    }
}
