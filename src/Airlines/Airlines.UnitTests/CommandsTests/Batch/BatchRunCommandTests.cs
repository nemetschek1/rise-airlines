﻿using Airlines.Business.Commands.Batch;

namespace Airlines.UnitTests.CommandsTests.Batch
{
    public class BatchRunCommandTests
    {
        [Fact]
        public void Execute_Successfully()
        {
            var batchData = new BatchData
            {
                IsBatchModeActive = true
            };

            var runData = new RunData(false);

            batchData.AddCommand(() => runData.WasExecuted = true);

            var batchStartCommand = BatchRunCommand.Create(batchData);

            batchStartCommand.Execute();

            Assert.True(runData.WasExecuted);
        }

        private class RunData(bool wasExecuted)
        {
            public bool WasExecuted { get; set; } = wasExecuted;
        }
    }
}
