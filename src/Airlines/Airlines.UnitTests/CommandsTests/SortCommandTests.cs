﻿using Airlines.Business.Commands;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airports;

namespace Airlines.UnitTests.CommandsTests
{
    public class SortCommandTests
    {
        [Fact]
        public void Execute_Sorts_Successfully()
        {
            var sortingType = BusinessDomainObjectsTypes.Airport;
            var sortingCollection = new InMemoryAirportRepository();
            var repositoryContainer = new RepositoryContainer()
            {
                Airports = sortingCollection,
            };

            var five = new Airport("5");
            var four = new Airport("4");
            var three = new Airport("3");

            sortingCollection.Add(five);
            sortingCollection.Add(four);
            sortingCollection.Add(three);

            var expectedResult = $"{sortingType}s:{Environment.NewLine}"
                + string.Join(Environment.NewLine, new List<BusinessDomainObject>() { three, four, five })
                + Environment.NewLine;

            var sortCommand = SortCommand.Create(["sort", $"{sortingType}s", "ascending"], repositoryContainer);

            sortCommand.Execute();

            Assert.Equal(expectedResult.Trim(), sortCommand.Output?.Trim());
        }
    }
}
