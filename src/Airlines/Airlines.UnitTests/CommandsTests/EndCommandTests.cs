﻿using Airlines.Business.Commands;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Models.Aircrafts;

namespace Airlines.UnitTests.CommandsTests
{
    public class EndCommandTests
    {
        [Fact]
        public void Execute_WithEmptyContainer_Successfully()
        {
            var expectedOutput = $"Airports:{Environment.NewLine}Airlines:{Environment.NewLine}Flights:{Environment.NewLine}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var endCommand = EndCommand.Create(repositoryContainer);

            endCommand.Execute();
            var output = endCommand.Output;

            Assert.False(endCommand.ContinueProgramAfterExecution);
            Assert.Equal(expectedOutput, output);
        }

        [Fact]
        public void Execute_WithSeededContainer_OneOfEachType_Successfully()
        {
            var airport = new Airport("aa", "a a", "2a", "2");
            var airline = new Airline("13.@lk");
            var emptyAircraft = new CargoAircraft(string.Empty, 0, 0);

            var departureAirport = new Airport("az");
            var arrivalAirport = new Airport("za");

            var flight = new Flight("aaz2", departureAirport, arrivalAirport, emptyAircraft, 1, 1);

            Func<object, string> getCollectionPrintResult = (collection) => $"{Environment.NewLine}{collection}{Environment.NewLine}";
            var expectedOutput = $"Airports:{getCollectionPrintResult(airport)}Airlines:{getCollectionPrintResult(airline)}Flights:{getCollectionPrintResult(flight)}";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            var endCommand = EndCommand.Create(repositoryContainer);

            repositoryContainer.Airports.Add(airport);
            repositoryContainer.Airlines.Add(airline);
            repositoryContainer.Flights.Add(flight);

            endCommand.Execute();
            var output = endCommand.Output;

            Assert.False(endCommand.ContinueProgramAfterExecution);
            Assert.Equal(expectedOutput.Trim(), output?.Trim());
        }
    }
}
