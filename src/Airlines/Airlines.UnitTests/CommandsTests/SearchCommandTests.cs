﻿using Airlines.Business.Commands;
using Airlines.Console.DependencyInversion;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Container.Seeding;

namespace Airlines.UnitTests.CommandsTests
{
    public class SearchCommandTests
    {
        [Theory]
        [InlineData("JFK", BusinessDomainObjectsTypes.Airport)]
        [InlineData("LAX", BusinessDomainObjectsTypes.Airport)]
        [InlineData("ORD", BusinessDomainObjectsTypes.Airport)]

        [InlineData("Da", BusinessDomainObjectsTypes.Airline)]

        [InlineData("FL123", BusinessDomainObjectsTypes.Flight)]
        [InlineData("FL456", BusinessDomainObjectsTypes.Flight)]
        [InlineData("FL789", BusinessDomainObjectsTypes.Flight)]
        public void Execute_SearchIsFound(
            string searchedTerm,
            BusinessDomainObjectsTypes answerType)
        {
            var expectedOutput = $"The 'search term' was found in {answerType}s.";

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();

            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);

            var searchCommand = SearchCommand.Create(["search", searchedTerm], repositoryContainer);

            searchCommand.Execute();

            Assert.Equal(expectedOutput, searchCommand.Output);
        }

        [Fact]
        public void Execute_SearchNotFound()
        {
            var expectedOutput = "The 'search term' was NOT found!";
            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();

            var searchCommand = SearchCommand.Create(["search", "not found"], repositoryContainer);

            searchCommand.Execute();

            Assert.Equal(expectedOutput, searchCommand.Output);
        }
    }
}
