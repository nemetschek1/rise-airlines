﻿using Airlines.Business.Commands;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airlines;

namespace Airlines.UnitTests.CommandsTests
{
    public class ExistsCommandTests
    {
        [Fact]
        public void Execute_ReturnsTrue()
        {
            var airline = new Airline("AAA");
            var inputPartsArray = new string[] { "exists", airline.Name ?? string.Empty };

            var airlineRepository = new InMemoryAirlineRepository();
            var repositoryContainer = new RepositoryContainer() { Airlines = airlineRepository };
            airlineRepository.Add(airline);

            var existsCommand = ExistsCommand.Create(inputPartsArray, repositoryContainer);

            existsCommand.Execute();
            var result = bool.Parse(existsCommand.Output ?? "False");

            Assert.True(result);
        }

        [Fact]
        public void Execute_ReturnsFalse()
        {
            var airline = new Airline("AAA");
            var inputPartsArray = new string[] { "exists", "123" };

            var airlineRepository = new InMemoryAirlineRepository();
            var repositoryContainer = new RepositoryContainer() { Airlines = airlineRepository };
            airlineRepository.Add(airline);

            var existsCommand = ExistsCommand.Create(inputPartsArray, repositoryContainer);

            existsCommand.Execute();
            var result = bool.Parse(existsCommand.Output ?? "False");

            Assert.False(result);
        }
    }
}
