﻿using Airlines.Business.Commands;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.Business.Commands.Utils;

namespace Airlines.UnitTests.CommandsTests
{
    public class ListCommandTests
    {
        [Theory]
        [InlineData(nameof(Airport.City))]
        [InlineData(nameof(Airport.Country))]
        public void Execute_WhereNoObjectsSearchedExist_ReturnsNotFoundMessage(string from)
        {
            var expectedMessage = $"{nameof(Airport)}s with such '{from}' doesn't exist!";

            var airportRepository = new InMemoryAirportRepository();
            var repositoryContainer = new RepositoryContainer() { Airports = airportRepository };

            var listCommand = ListCommand.Create(["list", "cant be found item", from], repositoryContainer);

            listCommand.Execute();

            Assert.Equal(expectedMessage, listCommand.Output);
        }

        [Theory]
        [InlineData(nameof(Airport.City), "New York")]
        [InlineData(nameof(Airport.Country), "USA")]
        public void Execute_WhereObjectsSearchedExist_ReturnsFoundMessage(string from, string value)
        {
            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);

            var expectedMessage = OutputFormater.FormatBusinessDomainObjectsOutput(
                BusinessDomainObjectsTypes.Airport, repositoryContainer.Airports.All());

            var listCommand = ListCommand.Create(["list", value, from], repositoryContainer);

            listCommand.Execute();

            Assert.Equal(expectedMessage, listCommand.Output);
        }
    }
}
