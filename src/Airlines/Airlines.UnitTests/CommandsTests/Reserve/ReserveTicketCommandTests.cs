﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Route;
using Airlines.Console.DependencyInversion;
using Airlines.Persistence.Basic.Container.Seeding;
using Airlines.Persistence.Basic.Models.Aircrafts;
using Airlines.UnitTests.Utils;

namespace Airlines.UnitTests.CommandsTests.Reserve
{
    public class ReserveTicketCommandTests
    {
        [Fact]
        public void Execute_WithValidFlightWithValidAircraft_Successfully()
        {
            var observedFlightIdentifier = "FL303";
            uint expectedSeats = 138;
            var expectedCargoWeight = 19_925;
            var expectedCargoVolume = 37.175;

            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedInMemoryRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "read", "airports", "./Resources/airports.csv");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "read", "aircrafts", "./Resources/aircrafts.csv");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "read", "flights", "./Resources/flights.csv");
            CommandUtils.ExecuteCommand(repositoryContainer, route, batchData, "reserve", "ticket", observedFlightIdentifier, "12", "1", "2");

            var flight = repositoryContainer.Flights.FindByKey(observedFlightIdentifier);
            var aircraft = flight!.Aircraft as PassengerAircraft;

            Assert.Equal(expectedSeats, aircraft!.Seats);
            Assert.Equal(expectedCargoWeight, aircraft!.Cargo.Weight);
            Assert.Equal(expectedCargoVolume, aircraft!.Cargo.Volume);
        }
    }
}
