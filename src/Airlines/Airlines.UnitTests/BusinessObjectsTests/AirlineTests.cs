﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Validation;

namespace Airlines.UnitTests.BusinessObjectsTests
{
    public class AirlineTests
    {
        [Theory]
        [InlineData("AAA")]
        [InlineData("...")]
        [InlineData(".....")]
        [InlineData("1Az.2")]
        [InlineData(".A2af")]
        public void ValidateInput_TestValidValues_ReturnsTrue(
            string airlineName)
            => Utils.ValidationUtils.TestThatWorksSuccessfully(GetInitializationClassFunc(airlineName));

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("  ")]
        [InlineData("   ")]
        [InlineData("    ")]
        [InlineData("......")]
        [InlineData("2r1-2-01r.")]
        [InlineData("123456")]
        [InlineData("abcdefg")]
        public void ValidateInput_TestInvalidValues_ReturnsFalse(
            string input)
            => Utils.ValidationUtils.TestThatFails(GetInitializationClassFunc(input));

        private static Action GetInitializationClassFunc(string airlineName)
        {
            var airline = new Airline(airlineName);

            return () => AirlineValidationHandler.Validate(airline);
        }
    }
}
