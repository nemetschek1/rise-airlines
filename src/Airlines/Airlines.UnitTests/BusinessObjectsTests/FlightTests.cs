﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Models.Aircrafts;
using Airlines.Persistence.Basic.Validation;

namespace Airlines.UnitTests.BusinessObjectsTests
{
    public class FlightTests
    {
        [Theory]
        [InlineData("a", "JFK", "LAX")]
        [InlineData("1", "Fas as", "a ss")]
        [InlineData("aa2", "Okw az", "az z ")]
        [InlineData("a2ffV", "a a", "aa aa")]
        [InlineData("zva", "AA", "a")]
        [InlineData("12214", "az Sim", "Sim az")]
        [InlineData("jk43Fsfaa", "Pop az", "az az")]
        [InlineData("jk43Fasaffasasf2r2152112512sfaa", "aa aa", "Aa AA")]
        public void ValidateInput_TestValidValues_ReturnsTrue(
            string identifier, string departureAirport, string arrivalAirport)
            => Utils.ValidationUtils.TestThatWorksSuccessfully(
                GetInitializationClassFunc(identifier, departureAirport, arrivalAirport));

        [Theory]
        [InlineData("", "", "1")]
        [InlineData(".", ".", ".1")]
        [InlineData(".rq2", "12", "21")]
        [InlineData(".%aff2", "$", ".a1")]
        public void ValidateInput_TestInalidValues_ReturnsFalse(
            string identifier, string departureAirport, string arrivalAirport)
            => Utils.ValidationUtils.TestThatFails(
                GetInitializationClassFunc(identifier, departureAirport, arrivalAirport));

        private static Action GetInitializationClassFunc(
            string identifier, string departureAirportName, string arrivalAirportName)
        {
            var emptyAircraft = new CargoAircraft(string.Empty, 0, 0);

            var departureAirport = new Airport(departureAirportName);
            var arrivalAirport = new Airport(arrivalAirportName);

            var flight = new Flight(identifier, departureAirport, arrivalAirport, emptyAircraft, 1, 1);

            return () => FlightValidationHandler.Validate(flight);
        }
    }
}
