﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Validation;

namespace Airlines.UnitTests.BusinessObjectsTests
{
    public class AirportTests
    {
        [Theory]
        [InlineData("Az23", "AjA", "A u", "e z")]
        [InlineData("aa", "aaa", "Az a", "aaaf sfas f")]
        [InlineData("23", "zva", "k k k", "a fs a")]
        [InlineData("A22B", "AZg", " a a", " a a ")]
        public void ValidateInput_TestValidValues_ReturnsTrue(
            string identifier, string name, string city, string country)
            => Utils.ValidationUtils.TestThatWorksSuccessfully(
                GetInitializationClassFunc(identifier, name, city, country));

        [Theory]
        [InlineData("", "", "", "")]
        [InlineData("a2", " ", "a a", "2 a")]
        [InlineData("frqwr", "..", "a a", "Afk")]
        [InlineData(",.2rf", "..f", ".a", "w2")]
        [InlineData(".f", "..G", "a a", "a a")]
        [InlineData("......", "1Az32", " ", "b")]
        [InlineData("aa", ".A2f", "az", "b2b")]
        [InlineData("aA", "aD991", "Az", "Z2")]
        public void ValidateInput_TestInvalidValues_ReturnsFalse(
            string identifier, string name, string city, string country)
            => Utils.ValidationUtils.TestThatFails(
                GetInitializationClassFunc(identifier, name, city, country));

        private static Action GetInitializationClassFunc(
            string identifier, string name, string city, string country)
        {
            var airport = new Airport(identifier, name, city, country);

            return () => AirportValidationHandler.Validate(airport);
        }
    }
}
