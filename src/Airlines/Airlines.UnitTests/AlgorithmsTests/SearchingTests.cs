﻿using Airlines.DataContracts.Enums;
using Airlines.UnitTests.Utils;
using Airlines.Business.Algorithms.Search;

using Xunit.Abstractions;

namespace Airlines.UnitTests.AlgorithmsTests
{
    public class SearchingTests(ITestOutputHelper output)
    {
        public const int ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS = 1;

        private readonly ITestOutputHelper _output = output;

        [Theory]
        [InlineData([new[] { "a" }, "a"])]
        [InlineData([new[] { "1", "2", "3" }, "1"])]
        [InlineData([new[] { "b", "1", "2", "3", "a" }, "3"])]
        public void UseLinearSearch_ForExistingSearchedItem_ReturnsTrue(string[] array, string searchedItem)
        {
            var result = array.Exists(searchedItem, SearchTypes.Linear);

            Assert.True(result);
        }

        [Theory]
        [InlineData([new string[] { }, "a"])]
        [InlineData([new[] { "1", "2", "3" }, "4"])]
        [InlineData([new[] { "b", "1", "2", "a" }, "3"])]
        [InlineData([new[] { "b", "1", "2", "2", "a" }, "-2"])]
        public void UseLinearSearch_ForNotExistingSearchedItem_ReturnsFalse(string[] array, string searchedItem)
        {
            var result = array.Exists(searchedItem, SearchTypes.Linear);

            Assert.False(result);
        }

        [Theory]
        [InlineData([new[] { "a" }, "a"])]
        [InlineData([new[] { "1", "2", "3" }, "1"])]
        [InlineData([new[] { "b", "1", "2", "3", "a" }, "3"])]
        public void UseBinarySearch_ForExistingSearchedItem_ReturnsTrue(string[] array, string searchedItem)
        {
            Array.Sort(array);

            var result = array.Exists(searchedItem, SearchTypes.Binary);

            Assert.True(result);
        }

        [Theory]
        [InlineData([new string[] { }, "a"])]
        [InlineData([new[] { "1", "2", "3" }, "4"])]
        [InlineData([new[] { "b", "1", "2", "a" }, "3"])]
        [InlineData([new[] { "b", "1", "2", "3", "a" }, null])]
        public void UseBinarySearch_ForNotExistingSearchedItem_ReturnsFalse(string[] array, string searchedItem)
        {
            Array.Sort(array);

            var result = array.Exists(searchedItem, SearchTypes.Binary);

            Assert.False(result);
        }

        [Fact]
        public void ComparePerformanceOfLinearSearchAndBinarySearch()
        {
            var array = SortTestingUtils.GenerateRandomArray();

            var searchedItemIndex = PerformanceUtils.GetRandomIndexInArray(array.Length);
            var searchedItem = array[searchedItemIndex];

            var iterationsLinearSearch = PerformanceUtils.CountIterationsOfFunctionForGivenPeriodOfTime(
                 ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS, (sw) => array.Exists(searchedItem, SearchTypes.Linear));

            Array.Sort(array);
            Assert.True(array.IsSortedAscending());

            var iterationsBinarySearch = PerformanceUtils.CountIterationsOfFunctionForGivenPeriodOfTime(
                ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS, (sw) => array.Exists(searchedItem, SearchTypes.Binary));

            _output.WriteLine($"Iterations Linear Search: {iterationsLinearSearch}");
            _output.WriteLine($"Iterations Binary Search: {iterationsBinarySearch}");
        }
    }
}
