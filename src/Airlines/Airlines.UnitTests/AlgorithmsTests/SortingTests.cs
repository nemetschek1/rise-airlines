﻿using Airlines.Persistence.Basic.Algorithms.Sort;
using Airlines.UnitTests.Utils;

using Xunit.Abstractions;

namespace Airlines.UnitTests.AlgorithmsTests
{
    public class SortingTests(ITestOutputHelper output)
    {
        public const int ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS = 1;

        private readonly ITestOutputHelper _output = output;

        [Theory]
        [InlineData([new[] { "a" }])]
        [InlineData([new[] { "a", "a" }])]
        [InlineData([new[] { "a", "b" }])]
        [InlineData([new[] { "a", "a", "b", "b" }])]
        [InlineData([new[] { "b", "a" }])]
        [InlineData([new[] { "b", "b", "a", "a" }])]
        [InlineData([new[] { "4", "2" }])]
        [InlineData([new[] { "4", "2", "1" }])]
        [InlineData([new[] { "1", "2", "1" }])]
        public void BubbleSort_SortsCorrectly(string[] array)
        {
            var bubbleSort = new BubbleSort();
            var sortedArray = bubbleSort.Execute(array).ToArray();
            var result = sortedArray.IsSortedAscending();

            Assert.True(result);
        }

        [Theory]
        [InlineData([new[] { "a" }])]
        [InlineData([new[] { "a", "a" }])]
        [InlineData([new[] { "a", "b" }])]
        [InlineData([new[] { "a", "a", "b", "b" }])]
        [InlineData([new[] { "b", "a" }])]
        [InlineData([new[] { "b", "b", "a", "a" }])]
        [InlineData([new[] { "4", "2" }])]
        [InlineData([new[] { "4", "2", "1" }])]
        [InlineData([new[] { "1", "2", "1" }])]
        public void SelectionSort_SortsCorrectly(string[] array)
        {
            var selectionSort = new SelectionSort();
            var sortedArray = selectionSort.Execute(array).ToArray();
            var result = sortedArray.IsSortedAscending();

            Assert.True(result);
        }

        [Fact]
        public void ComparePerformanceOfBubbleSortAndSelectionSort()
        {
            var array = SortTestingUtils.GenerateRandomArray();

            var iterationsBubbleSort = PerformanceUtils.CountIterationsOfFunctionForGivenPeriodOfTime(
                 ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS,
                 (sw) => PerformanceUtils.UseSortOnNewArray(sw, array, new BubbleSort()));

            var iterationsSelectionSort = PerformanceUtils.CountIterationsOfFunctionForGivenPeriodOfTime(
                ALLOWED_TIME_IN_SECONDS_TO_EXECUTE_METHODS,
                (sw) => PerformanceUtils.UseSortOnNewArray(sw, array, new SelectionSort()));

            _output.WriteLine($"Iterations Bubble Sort: {iterationsBubbleSort}");
            _output.WriteLine($"Iterations Selection Sort: {iterationsSelectionSort}");
        }
    }
}
