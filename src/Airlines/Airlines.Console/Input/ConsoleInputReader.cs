﻿using Airlines.Business.Commands;
using Airlines.Business.Commands.Utils;
using Airlines.Business.Route;
using Airlines.Business.Commands.Batch;
using Airlines.Console.DependencyInversion;
using Airlines.Console.Output;
using Airlines.Persistence.Basic.Container.Seeding;

namespace Airlines.Console.Input
{
    public static class ConsoleInputReader
    {
        public static void ReadInput()
        {
            var repositoryContainer = DependencyContainer.CreateInMemoryRepositoryContainer();
            DataSeeder.SeedDbRepositoryContainer(repositoryContainer);
            var route = new RouteGraph(repositoryContainer.RouteFlights);
            var batchData = new BatchData();

            var continueExecution = true;
            do
            {
                string? output = null;
                try
                {
                    var input = ReadLineFromUser();
                    var inputParts = input.Split();

                    var command = CommandFactory.Create(inputParts, repositoryContainer, route, batchData);

                    if (batchData.IsBatchModeActive)
                    {
                        batchData.AddCommand(command.Execute);
                    }
                    else
                    {
                        command.Execute();

                        continueExecution = command.ContinueProgramAfterExecution;
                        output = command.Output;
                    }
                }
                catch (Exception e)
                {
                    output = OutputFormater.FormatErrorForUser(e.Message);
                }
                finally
                {
                    if (!string.IsNullOrEmpty(output))
                    {
                        OutputPrinter.PrintOutputToUser(output);
                    }
                }
            } while (continueExecution);
        }

        private static string ReadLineFromUser()
        {
            var inputLine = System.Console.ReadLine();

            if (inputLine != null)
                return inputLine;

            return string.Empty;
        }
    }
}
