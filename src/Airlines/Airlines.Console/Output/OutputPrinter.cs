﻿namespace Airlines.Console.Output
{
    public static class OutputPrinter
    {
        public static string PrintOutputToUser(string text)
        {
            System.Console.WriteLine(text);

            return text;
        }
    }
}
