﻿using AutoMapper;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Repository.Aircrafts;
using Airlines.Persistence.Basic.Repository.Airlines;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;
using Airlines.Persistence.Basic.Repository.RouteFlights;
using Microsoft.Extensions.Configuration;
using Airlines.Business.MappingProfiles;

namespace Airlines.Console.DependencyInversion
{
    public static class DependencyContainer
    {
        internal const string CONFIGURATIONS_FILE = "appsettings.json";

        public static IConfigurationRoot CreateConfiguration()
            => new ConfigurationBuilder()
                .AddJsonFile(CONFIGURATIONS_FILE)
                .Build();

        private static ApplicationDbContext CreateDbContext()
            => new();

        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AirportProfile>();
                cfg.AddProfile<AirlineProfile>();
                cfg.AddProfile<FlightProfile>();
            });

            return new Mapper(config);
        }

        public static RepositoryContainer CreateDatabaseRepositoryContainer()
        {
            var dbContext = CreateDbContext();
            var mapper = CreateMapper();

            var flightRepository = new ConsoleDbFlightRepository(dbContext, mapper);
            var airportRepository = new ConsoleDbAirportRepository(dbContext, mapper);
            var aircraftRepository = new InMemoryAircraftRepository();
            var airlineRepository = new ConsoleDbAirlineRepository(dbContext, mapper);
            var routeFlightRepository = new InMemoryRouteFlightRepository();

            return new RepositoryContainer(flightRepository,
                airportRepository,
                aircraftRepository,
                airlineRepository,
                routeFlightRepository);
        }

        public static RepositoryContainer CreateInMemoryRepositoryContainer()
        {
            var flightRepository = new InMemoryFlightRepository();
            var airportRepository = new InMemoryAirportRepository();
            var aircraftRepository = new InMemoryAircraftRepository();
            var airlineRepository = new InMemoryAirlineRepository();
            var routeFlightRepository = new InMemoryRouteFlightRepository();

            return new RepositoryContainer(flightRepository,
                airportRepository,
                aircraftRepository,
                airlineRepository,
                routeFlightRepository);
        }
    }
}
