﻿using Airlines.Console.Input;

namespace Airlines.Console
{
    public class Program
    {
        public static void Main() => ConsoleInputReader.ReadInput();
    }
}
