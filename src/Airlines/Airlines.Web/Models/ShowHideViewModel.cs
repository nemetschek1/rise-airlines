﻿namespace Airlines.Web.Models
{
    public class ShowHideViewModel(string showHideElId, string showHideBtnNameExtension)
    {
        public string ShowHideElId { get; set; } = showHideElId;

        public string ShowHideBtnNameExtension { get; set; } = showHideBtnNameExtension;
    }
}
