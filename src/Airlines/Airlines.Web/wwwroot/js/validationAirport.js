﻿window.addEventListener("load", () => {
    const formEl = document.querySelector("#add-section form");
    const nameInputEl = document.getElementById("input_Name");
    const countryInputEl = document.getElementById("input_Country");
    const cityInputEl = document.getElementById("input_City");
    const codeInputEl = document.getElementById("input_Code");
    const runwaysCountInputEl = document.getElementById("input_RunwaysCount");
    const foundedInputEl = document.getElementById("input_Founded");
    const submitBtnEl = formEl.querySelector("input[type='submit']");

    submitBtnEl.setAttribute("disabled", true);

    formEl.addEventListener("change", validateForm);
    nameInputEl.addEventListener("blur", validateName);
    countryInputEl.addEventListener("blur", validateCountry);
    cityInputEl.addEventListener("blur", validateCity);
    codeInputEl.addEventListener("blur", validateCode);
    runwaysCountInputEl.addEventListener("blur", validateRunwaysCount);
    foundedInputEl.addEventListener("blur", validateFounded);

    function validateForm() {
        const isFormValid = validateName(false) &&
            validateCountry(false) &&
            validateCity(false) &&
            validateCode(false) &&
            validateRunwaysCount(false) &&
            validateFounded(false);

        if (isFormValid) {
            submitBtnEl.removeAttribute("disabled");
        }
        else {
            submitBtnEl.setAttribute("disabled", true);
        }
    }

    function validateName(showError = true) {
        const errorContainerEl = showError ?
            nameInputEl.nextElementSibling : document.createElement("span");

        if (nameInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (nameInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateCountry(showError = true) {
        const errorContainerEl = showError ?
            countryInputEl.nextElementSibling : document.createElement("span");

        if (countryInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (countryInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateCity(showError = true) {
        const errorContainerEl = showError ?
            cityInputEl.nextElementSibling : document.createElement("span");

        if (cityInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (cityInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateCode(showError = true) {
        const errorContainerEl = showError ?
            codeInputEl.nextElementSibling : document.createElement("span");

        if (codeInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (codeInputEl.value.length > 4) {
            errorContainerEl.textContent = "Length should be less than 4!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateRunwaysCount(showError = true) {
        const errorContainerEl = showError ?
            runwaysCountInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (Number(runwaysCountInputEl.value) < 1) {
            errorContainerEl.textContent = "Should be more than 0!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateFounded(showError = true) {
        const errorContainerEl = showError ?
            foundedInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (new Date(foundedInputEl.value) < new Date("01/01/1976")) {
            errorContainerEl.textContent = "Should be greater than 01.01.1976!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }
});