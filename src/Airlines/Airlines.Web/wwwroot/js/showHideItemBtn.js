﻿window.addEventListener("load", () => {
    const btnId = "showHideBtn";
    const btnAttributeStoringShowHideElId = "watch-for";
    const hiddenClassName = "hidden";
    let btnShowText = "Show";
    let btnHideText = "Hide";

    const showHideBtnEl = document.getElementById(btnId);

    const showHideBtnNameExtension = " " + showHideBtnEl.textContent.trim();
    btnHideText += showHideBtnNameExtension;
    btnShowText += showHideBtnNameExtension;

    const showHideElId = showHideBtnEl.getAttribute(btnAttributeStoringShowHideElId);
    const showHideEl = document.getElementById(showHideElId);

    const visibilityChangingFunc = setVisibility.bind(null, showHideBtnEl, showHideEl);

    visibilityChangingFunc();
    showHideBtnEl.addEventListener("click", visibilityChangingFunc);

    function setVisibility(showHideBtnEl, showHideEl) {
        if (showHideBtnEl.textContent == btnHideText) {
            showHideBtnEl.textContent = btnShowText;
            showHideEl.classList.add(hiddenClassName);
            return;
        }

        showHideEl.classList.remove(hiddenClassName);
        showHideBtnEl.textContent = btnHideText;
    }
});