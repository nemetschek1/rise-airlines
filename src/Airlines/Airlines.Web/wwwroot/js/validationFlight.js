﻿window.addEventListener("load", () => {
    const formEl = document.querySelector("#add-section form");
    const numberInputEl = document.getElementById("input_Number");
    const fromInputEl = document.getElementById("input_From");
    const toInputEl = document.getElementById("input_To");
    const departureInputEl = document.getElementById("input_Departure");
    const arrivalInputEl = document.getElementById("input_Arrival");
    const submitBtnEl = formEl.querySelector("input[type='submit']");

    submitBtnEl.setAttribute("disabled", true);

    formEl.addEventListener("change", validateForm);
    numberInputEl.addEventListener("blur", validateNumber);
    fromInputEl.addEventListener("blur", validateFrom);
    toInputEl.addEventListener("blur", validateTo);
    departureInputEl.addEventListener("blur", validateDeparture);
    arrivalInputEl.addEventListener("blur", validateArrival);

    function validateForm() {
        const isFormValid = validateNumber(false) &&
            validateFrom(false) &&
            validateTo(false) &&
            validateDeparture(false) &&
            validateArrival(false);

        if (isFormValid) {
            submitBtnEl.removeAttribute("disabled");
        }
        else {
            submitBtnEl.setAttribute("disabled", true);
        }
    }

    function validateNumber(showError = true) {
        const errorContainerEl = showError ?
            numberInputEl.nextElementSibling : document.createElement("span");

        if (numberInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (numberInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateFrom(showError = true) {
        const errorContainerEl = showError ?
            fromInputEl.nextElementSibling : document.createElement("span");

        if (fromInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (fromInputEl.value.length > 4) {
            errorContainerEl.textContent = "Length should be less than 4!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateTo(showError = true) {
        const errorContainerEl = showError ?
            toInputEl.nextElementSibling : document.createElement("span");

        if (toInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (toInputEl.value.length > 4) {
            errorContainerEl.textContent = "Length should be less than 4!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateDeparture(showError = true) {
        const errorContainerEl = showError ?
            departureInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (new Date(departureInputEl.value) < Date.now()) {
            errorContainerEl.textContent = "Should be greater than now!";
            return false;
        }

        if (arrivalInputEl.value != "") {
            if (new Date(departureInputEl.value) >= new Date(arrivalInputEl.value)) {
                errorContainerEl.textContent = "Departure should be less than arrival!";
                return false;
            }
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateArrival(showError = true) {
        const errorContainerEl = showError ?
            arrivalInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (new Date(arrivalInputEl.value) < Date.now()) {
            errorContainerEl.textContent = "Should be greater than now!";
            return false;
        }

        if (departureInputEl.value != "") {
            if (new Date(departureInputEl.value) >= new Date(arrivalInputEl.value)) {
                errorContainerEl.textContent = "Arrival should be biggerr than departure!";
                return false;
            }
        }

        errorContainerEl.textContent = "";
        return true;
    }
});