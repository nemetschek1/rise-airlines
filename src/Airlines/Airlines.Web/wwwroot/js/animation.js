﻿window.addEventListener("load", () => {
    const goUpBtn = document.createElement("button");
    goUpBtn.textContent = "Go Up";
    goUpBtn.id = "goUpBtn";

    const mainEl = document.querySelector("main div");
    mainEl.appendChild(goUpBtn);

    goUpBtn.addEventListener("click", topFunction);
    window.onscroll = function () { scrollFunction() };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            goUpBtn.style.display = "block";
        } else {
            goUpBtn.style.display = "none";
        }
    }

    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
});