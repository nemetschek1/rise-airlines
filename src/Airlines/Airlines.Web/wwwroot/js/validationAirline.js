﻿window.addEventListener("load", () => {
    const formEl = document.querySelector("#add-section form");
    const nameInputEl = document.getElementById("input_Name");
    const foundedInputEl = document.getElementById("input_Founded");
    const fleetSizeInputEl = document.getElementById("input_FleetSize");
    const descriptionInputEl = document.getElementById("input_Description");
    const submitBtnEl = formEl.querySelector("input[type='submit']");

    submitBtnEl.setAttribute("disabled", true);

    formEl.addEventListener("change", validateForm);
    nameInputEl.addEventListener("blur", validateName);
    foundedInputEl.addEventListener("blur", validateFounded);
    fleetSizeInputEl.addEventListener("blur", validateFleetSize);
    descriptionInputEl.addEventListener("blur", validateDescription);

    function validateForm() {
        const isFormValid = validateName(false) &&
            validateFounded(false) &&
            validateFleetSize(false) &&
            validateDescription(false);

        if (isFormValid) {
            submitBtnEl.removeAttribute("disabled");
        }
        else {
            submitBtnEl.setAttribute("disabled", true);
        }
    }

    function validateName(showError = true) {
        const errorContainerEl = showError ?
            nameInputEl.nextElementSibling : document.createElement("span");

        if (nameInputEl.value.length == 0) {
            errorContainerEl.textContent = "Field is required!";
            return false;
        }

        if (nameInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateFounded(showError = true) {
        const errorContainerEl = showError ?
            foundedInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (new Date(foundedInputEl.value) < new Date("11/16/1909")) {
            errorContainerEl.textContent = "Should be greater than 16.11.1909!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateFleetSize(showError = true) {
        const errorContainerEl = showError ?
            fleetSizeInputEl.nextElementSibling.nextElementSibling : document.createElement("span");

        if (Number(fleetSizeInputEl.value) < 1) {
            errorContainerEl.textContent = "Should be more than 0!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }

    function validateDescription(showError = true) {
        const errorContainerEl = showError ?
            descriptionInputEl.nextElementSibling : document.createElement("span");

        if (descriptionInputEl.value.length > 256) {
            errorContainerEl.textContent = "Length should be less than 256!";
            return false;
        }

        errorContainerEl.textContent = "";
        return true;
    }
});