﻿window.addEventListener("load", () => {
    const btnId = "expandShrinkBtn";
    const btnAttributeStoringTableQuerySelector = "watch-for";
    const hiddenClassName = "hidden";
    const btnExpandText = "Expand";
    const btnShrinkText = "Shrink";

    const btnEl = document.getElementById(btnId);

    const tableElQuerySelector = btnEl.getAttribute(btnAttributeStoringTableQuerySelector);
    const tableEl = document.querySelector(tableElQuerySelector);

    const elementsForHidingShowing = Array.from(tableEl.getElementsByTagName("tr")).slice(3);

    if (elementsForHidingShowing.length > 0) {
        const tableChangingFunc = setTableState.bind(null, btnEl, elementsForHidingShowing);

        tableChangingFunc();
        btnEl.addEventListener("click", tableChangingFunc);
    } else {
        btnEl.classList.add(hiddenClassName);
    }

    function setTableState(btnEl, elementsForHidingShowing) {
        if (btnEl.textContent == btnShrinkText) {
            btnEl.textContent = btnExpandText;
            elementsForHidingShowing.forEach(x => x.classList.add(hiddenClassName));
            return;
        }

        btnEl.textContent = btnShrinkText;
        elementsForHidingShowing.forEach(x => x.classList.remove(hiddenClassName));
    }
});