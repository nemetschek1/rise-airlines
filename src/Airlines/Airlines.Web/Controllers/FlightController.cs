﻿using Airlines.Business.Models;
using Airlines.Business.Services.Flights;
using Airlines.Persistence.Basic.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class FlightController(IFlightService flightService) : BaseController
    {
        private readonly IFlightService _flightService = flightService;

        public IActionResult Index(SearchInputModel searchInputModel)
        {
            var flights = _flightService.GetAll(searchInputModel);

            return View(flights);
        }

        [HttpPost]
        public IActionResult Add(Flight flight)
        {
            HandleValidation();

            _flightService.Add(flight);

            return RedirectToAction(nameof(Index));
        }
    }
}
