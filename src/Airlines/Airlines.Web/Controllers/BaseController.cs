﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Web.Controllers
{
    public class BaseController : Controller
    {
        protected void HandleValidation()
        {
            if (!ModelState.IsValid)
            {
                var errorsAsText = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors));

                throw new ValidationException(errorsAsText);
            }
        }
    }
}
