﻿using Airlines.Business.Models;
using Airlines.Business.Services.Airports;
using Airlines.Persistence.Basic.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class AirportController(IAirportService airportService) : BaseController
    {
        private readonly IAirportService _airportService = airportService;

        public IActionResult Index(SearchInputModel searchInputModel)
        {
            var airports = _airportService.GetAll(searchInputModel);

            return View(airports);
        }

        [HttpPost]
        public IActionResult Add(Airport airport)
        {
            HandleValidation();

            _airportService.Add(airport);

            return RedirectToAction(nameof(Index));
        }
    }
}