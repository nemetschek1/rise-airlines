﻿using Airlines.Business.Models;
using Airlines.Business.Services.Airlines;
using Airlines.Persistence.Basic.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class AirlineController(IAirlineService airlineService) : BaseController
    {
        private readonly IAirlineService _airlineService = airlineService;

        public IActionResult Index(SearchInputModel searchInputModel)
        {
            var airlines = _airlineService.GetAll(searchInputModel);

            return View(airlines);
        }

        [HttpPost]
        public IActionResult Add(Airline airline)
        {
            HandleValidation();

            _airlineService.Add(airline);

            return RedirectToAction(nameof(Index));
        }
    }
}