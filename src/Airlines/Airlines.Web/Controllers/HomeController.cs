﻿using Airlines.Business.Services.Home;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class HomeController(IHomeService homeService) : Controller
    {
        private readonly IHomeService _homeService = homeService;

        public IActionResult Index()
        {
            var model = _homeService.GetHomeIndexPageViewModel();

            return View(model);
        }
    }
}