﻿namespace Airlines.Business.Route.RouteDeterminer
{
    public static class RouteDeterminerFactory
    {
        public static IRouteDeterminer Create(string strategy)
        {
            return strategy switch
            {
                "cheap" => new CheapestRouteDeterminer(),
                "short" => new FastestRouteDeterminer(),
                "stops" => new ShortestRouteDeterminer(),
                _ => throw new ArgumentException("Shortest route determining strategy is invalid!"),
            };
        }
    }
}
