﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Route.RouteDeterminer
{
    public interface IRouteDeterminer
    {
        public List<Airport>? Find(List<List<Airport>> routes);
    }
}