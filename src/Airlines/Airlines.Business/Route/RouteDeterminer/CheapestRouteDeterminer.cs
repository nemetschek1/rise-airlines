﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Route.RouteDeterminer
{
    internal class CheapestRouteDeterminer : IRouteDeterminer
    {
        public List<Airport>? Find(List<List<Airport>> routes)
        {
            var routesWithPrice = routes.ToDictionary(SumPriceOfRoute, x => x);

            var cheapestRoutePrice = routesWithPrice.Keys.Count > 0
                ? routesWithPrice.Keys.Min() : 0;

            routesWithPrice.TryGetValue(cheapestRoutePrice, out var minRoute);

            return minRoute;
        }

        private static decimal SumPriceOfRoute(List<Airport> route)
        {
            var price = 0M;
            for (var i = 1; i < route.Count; i++)
            {
                var flight = route[i - 1].FlightsFromCurrent
                    .First(x => x.ArrivalAirport == route[i]);

                price += flight.Price!.Value;
            }

            return price;
        }
    }
}