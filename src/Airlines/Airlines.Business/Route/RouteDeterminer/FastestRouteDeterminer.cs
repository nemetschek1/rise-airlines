﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Route.RouteDeterminer
{
    internal class FastestRouteDeterminer : IRouteDeterminer
    {
        public List<Airport>? Find(List<List<Airport>> routes)
        {
            var routesWithTime = routes.ToDictionary(SumTimeOfRoute, x => x);

            var fastestRouteTime = routesWithTime.Keys.Count > 0
                ? routesWithTime.Keys.Min() : 0;

            routesWithTime.TryGetValue(fastestRouteTime, out var minRoute);

            return minRoute;
        }

        private static double SumTimeOfRoute(List<Airport> route)
        {
            var time = 0.0;
            for (var i = 1; i < route.Count; i++)
            {
                var flight = route[i - 1].FlightsFromCurrent
                    .First(x => x.ArrivalAirport == route[i]);

                time += flight.DurationInHours!.Value;
            }

            return time;
        }
    }
}