﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Route.RouteDeterminer
{
    internal class ShortestRouteDeterminer : IRouteDeterminer
    {
        public List<Airport>? Find(List<List<Airport>> routes)
        {
            var minRouteCount = routes.Count > 0 ? routes.Min(x => x.Count) : 0;
            var minRoute = routes.FirstOrDefault(x => x.Count == minRouteCount);

            return minRoute;
        }
    }
}