﻿using Airlines.Business.Route.RouteDeterminer;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.RouteFlights;
using System.Text;

namespace Airlines.Business.Route
{
    public class RouteGraph(IRouteFlightRepository<Flight> flightRepository)
    {
        private readonly IRouteFlightRepository<Flight> _flights = flightRepository;

        public void Add(Flight flight)
        {
            flight.DepartureAirport!.FlightsFromCurrent.Add(flight);

            _flights.Add(flight);
        }

        public void Remove(Flight flight)
        {
            flight.DepartureAirport!.FlightsFromCurrent.Remove(flight);

            _flights.Remove(flight);
        }

        public void Clear()
            => _flights.Clear();

        public int Count
            => _flights.Count;

        public bool DoesRouteToAirportExist(Airport? currentAirport, Airport? destinationAirport)
        {
            var paths = CallGenerateRoutesToAirport(currentAirport, destinationAirport, false);

            return paths.Count != 0;
        }

        public List<List<Airport>> FindAllRoutesToAirport(Airport? currentAirport, Airport? destinationAirport)
        {
            var paths = CallGenerateRoutesToAirport(currentAirport, destinationAirport, true);

            return paths;
        }

        public List<Airport>? FindShortestRouteToAirport(Airport? currentAirport,
           Airport? destinationAirport,
           IRouteDeterminer routeDeterminer)
        {
            var paths = CallGenerateRoutesToAirport(currentAirport, destinationAirport, true);

            return routeDeterminer.Find(paths);
        }

        private List<List<Airport>> CallGenerateRoutesToAirport(Airport? currentAirport,
            Airport? destinationAirport,
            bool findAll)
        {
            if (currentAirport == null || destinationAirport == null)
                return [];

            var visited = new HashSet<Airport>();
            var path = new List<Airport>();
            var paths = new List<List<Airport>>();

            GenerateAllRoutesBetweenTwoAirports(currentAirport, destinationAirport, visited, path, paths, findAll);

            return paths;
        }

        private bool GenerateAllRoutesBetweenTwoAirports(Airport currentAirport,
           Airport destinationAirport,
           HashSet<Airport> visited,
           List<Airport> path,
           List<List<Airport>> paths,
           bool findAll)
        {
            visited.Add(currentAirport);
            path.Add(currentAirport);

            if (currentAirport.Equals(destinationAirport))
            {
                paths.Add(new List<Airport>(path));

                if (!findAll)
                {
                    return true;
                }
            }
            else
            {
                foreach (var flight in currentAirport.FlightsFromCurrent)
                {
                    if (!_flights.Contains(flight))
                    {
                        continue;
                    }

                    var newCurrentAirport = flight.ArrivalAirport!;

                    if (!visited.Contains(newCurrentAirport))
                    {
                        var endProgram = GenerateAllRoutesBetweenTwoAirports(
                            newCurrentAirport, destinationAirport, visited, path, paths, findAll);

                        if (endProgram)
                        {
                            return true;
                        }
                    }
                }
            }

            path.Remove(currentAirport);
            visited.Remove(currentAirport);

            return false;
        }

        public override string ToString()
        {
            var resultBuilder = new StringBuilder();

            foreach (var flight in _flights.All())
                resultBuilder.AppendLine($"{flight.DepartureAirport?.Name} -> {flight.ArrivalAirport?.Name}");

            return resultBuilder.ToString();
        }
    }
}
