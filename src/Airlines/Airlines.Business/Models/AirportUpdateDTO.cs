﻿namespace Airlines.Business.Models
{
    public class AirportUpdateDTO
    {
        public string? Name { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Code { get; set; }

        public int RunwaysCount { get; set; }

        public DateOnly Founded { get; set; }
    }
}
