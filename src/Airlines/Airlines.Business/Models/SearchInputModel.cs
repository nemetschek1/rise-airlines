﻿namespace Airlines.Business.Models
{
    public class SearchInputModel
    {
        public string Category { get; set; }

        public string Keyword { get; set; }
    }
}
