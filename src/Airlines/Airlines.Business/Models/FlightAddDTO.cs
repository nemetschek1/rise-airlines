﻿namespace Airlines.Business.Models
{
    public class FlightAddDTO
    {
        public string Number { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public DateTime Departure { get; set; }

        public DateTime Arrival { get; set; }
    }
}
