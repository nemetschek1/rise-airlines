﻿namespace Airlines.Business.Models
{
    public class HomeIndexPageViewModel
    {
        public int AirlinesCount { get; set; }

        public int AirportsCount { get; set; }

        public int FlightsCount { get; set; }
    }
}
