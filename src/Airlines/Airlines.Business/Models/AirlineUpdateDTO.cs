﻿namespace Airlines.Business.Models
{
    public class AirlineAddDTO
    {
        public string Name { get; set; }

        public DateOnly Founded { get; set; }

        public int FleetSize { get; set; }

        public string? Description { get; set; }
    }
}
