﻿using AutoMapper;

namespace Airlines.Business.MappingProfiles
{
    public class FlightProfile : Profile
    {
        public FlightProfile()
        {
            CreateMap<Persistence.Basic.Entities.Flight, Persistence.Basic.Entities.Flight>();

            CreateMap<Persistence.Basic.Entities.Flight, Models.FlightAddDTO>().ReverseMap();
            CreateMap<Persistence.Basic.Entities.Flight, Models.FlightUpdateDTO>().ReverseMap();

            CreateMap<Persistence.Basic.Models.Flight, Persistence.Basic.Entities.Flight>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.Identifier))
                .ForMember(dest => dest.Arrival, opt => opt.MapFrom(src => src.ArrivalAirport))
                .ForMember(dest => dest.Departure, opt => opt.MapFrom(src => src.DepartureAirport))
                .ReverseMap();
        }
    }
}
