﻿using AutoMapper;

namespace Airlines.Business.MappingProfiles
{
    public class AirlineProfile : Profile
    {
        public AirlineProfile()
        {
            CreateMap<Persistence.Basic.Entities.Airline, Persistence.Basic.Entities.Airline>();

            CreateMap<Persistence.Basic.Entities.Airline, Models.AirlineAddDTO>().ReverseMap();
            CreateMap<Persistence.Basic.Entities.Airline, Models.AirlineUpdateDTO>().ReverseMap();

            CreateMap<Persistence.Basic.Models.Airline, Persistence.Basic.Entities.Airline>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ReverseMap();
        }
    }
}
