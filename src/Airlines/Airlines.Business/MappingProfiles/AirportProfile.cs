﻿using AutoMapper;

namespace Airlines.Business.MappingProfiles
{
    public class AirportProfile : Profile
    {
        public AirportProfile()
        {
            CreateMap<Persistence.Basic.Entities.Airport, Persistence.Basic.Entities.Airport>();

            CreateMap<Persistence.Basic.Entities.Airport, Models.AirportAddDTO>().ReverseMap();
            CreateMap<Persistence.Basic.Entities.Airport, Models.AirportUpdateDTO>().ReverseMap();

            CreateMap<Persistence.Basic.Models.Airport, Persistence.Basic.Entities.Airport>()
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Identifier))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.FlightFromNavigations, opt => opt.MapFrom(src => src.FlightsFromCurrent))
                .ReverseMap();
        }
    }
}
