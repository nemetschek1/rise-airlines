﻿using Airlines.DataContracts.Exceptions.Validation.Aircraft;
using Airlines.Persistence.Basic.Models.Aircrafts;

namespace Airlines.Business.FlightReserve
{
    public static class FlightReserver
    {
        public static void ReserveTicket(Aircraft aircraft, double cargoWeight, double cargoVolume, uint seats)
        {
            if (aircraft is not PassengerAircraft)
            {
                throw new InvalidAircraftMethodException();
            }

            var passengerAircraft = (PassengerAircraft)aircraft;

            passengerAircraft.Seats -= seats;
            passengerAircraft.Cargo.Weight -= cargoWeight;
            passengerAircraft.Cargo.Volume -= cargoVolume;
        }

        public static void ReserveCargo(Aircraft aircraft, double cargoWeight, double cargoVolume)
        {
            if (aircraft is not CargoAircraft)
            {
                throw new InvalidAircraftMethodException();
            }

            var passengerAircraft = (CargoAircraft)aircraft;

            passengerAircraft.Cargo.Weight -= cargoWeight;
            passengerAircraft.Cargo.Volume -= cargoVolume;
        }
    }
}