﻿using Airlines.Business.FlightReserve;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Business.Commands.Reserve
{
    public class ReserveCargoCommand : ICommand
    {
        private readonly string _flightIdentifier;
        private readonly double _cargoWeight;
        private readonly double _cargoVolume;
        private readonly IFlightRepository<Flight> _flightRepository;

        private ReserveCargoCommand(string flightIdentifier,
            double cargoWeight,
            double cargoVolume,
            IFlightRepository<Flight> flightRepository)
        {
            _flightIdentifier = flightIdentifier;
            _cargoWeight = cargoWeight;
            _cargoVolume = cargoVolume;
            _flightRepository = flightRepository;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static ReserveCargoCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var flightIdentifier = GetFlightIdentifierFromInputParts(inputParts);
            if (string.IsNullOrEmpty(flightIdentifier))
            {
                throw new ArgumentException("Flight Identifier is missing!");
            }

            var cargoWeightText = GetCargoWeightFromInputParts(inputParts);
            var cargoWeight = double.Parse(cargoWeightText);

            if (cargoWeight <= 0)
            {
                throw new ArgumentException("Cargo Weight needs to be positive!");
            }

            var cargoVolumeText = GetCargoVolumeFromInputParts(inputParts);
            var cargoVolume = double.Parse(cargoVolumeText);

            if (cargoVolume <= 0)
            {
                throw new ArgumentException("Cargo Volume needs to be positive!");
            }

            return new ReserveCargoCommand(flightIdentifier, cargoWeight, cargoVolume, repositoryContainer.Flights);
        }

        private static string GetFlightIdentifierFromInputParts(string[] inputParts, int index = 2)
          => inputParts[index];

        private static string GetCargoWeightFromInputParts(string[] inputParts, int index = 3)
          => inputParts[index];

        private static string GetCargoVolumeFromInputParts(string[] inputParts, int index = 4)
              => inputParts[index];

        public void Execute()
        {
            var flight = _flightRepository.FindByKey(_flightIdentifier);

            ArgumentNullException.ThrowIfNull(flight);
            ArgumentNullException.ThrowIfNull(flight.Aircraft);

            FlightReserver.ReserveCargo(flight.Aircraft, _cargoWeight, _cargoVolume);
        }
    }
}