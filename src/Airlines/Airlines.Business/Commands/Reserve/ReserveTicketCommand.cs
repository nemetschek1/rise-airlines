﻿using Airlines.Business.FlightReserve;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Business.Commands.Reserve
{
    public class ReserveTicketCommand : ICommand
    {
        private readonly string _flightIdentifier;
        private readonly uint _seats;
        private readonly uint _smallBaggageCount;
        private readonly uint _largeBaggageCount;
        private readonly IFlightRepository<Flight> _flightRepository;

        private ReserveTicketCommand(string flightIdentifier,
            uint seats,
            uint smallBaggageCount,
            uint largeBaggageCount,
            IFlightRepository<Flight> flightRepository)
        {
            _flightIdentifier = flightIdentifier;
            _seats = seats;
            _smallBaggageCount = smallBaggageCount;
            _largeBaggageCount = largeBaggageCount;
            _flightRepository = flightRepository;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static ReserveTicketCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var flightIdentifier = GetFlightIdentifierFromInputParts(inputParts);

            var seatsText = GetSeatsFromInputParts(inputParts);
            var seats = uint.Parse(seatsText);

            if (seats <= 0)
            {
                throw new ArgumentException("Seeds need to be positive!");
            }

            var smallBaggageCountText = GetSmallBaggageCountFromInputParts(inputParts);
            var smallBaggageCount = uint.Parse(smallBaggageCountText);

            if (smallBaggageCount <= 0)
            {
                throw new ArgumentException("Small baggage count needs to be positive!");
            }

            var largeBaggageCountText = GetLargeBaggageCountFromInputParts(inputParts);
            var largeBaggageCount = uint.Parse(largeBaggageCountText);

            if (largeBaggageCount <= 0)
            {
                throw new ArgumentException("Large baggage count needs to be positive!");
            }

            return new ReserveTicketCommand(flightIdentifier,
                seats,
                smallBaggageCount,
                largeBaggageCount,
                repositoryContainer.Flights);
        }

        private static string GetFlightIdentifierFromInputParts(string[] inputParts, int index = 2)
          => inputParts[index];

        private static string GetSeatsFromInputParts(string[] inputParts, int index = 3)
          => inputParts[index];

        private static string GetSmallBaggageCountFromInputParts(string[] inputParts, int index = 4)
              => inputParts[index];

        private static string GetLargeBaggageCountFromInputParts(string[] inputParts, int index = 5)
             => inputParts[index];

        public void Execute()
        {
            var flight = _flightRepository.FindByKey(_flightIdentifier);

            ArgumentNullException.ThrowIfNull(flight);

            var cargoWeight = CalculateCargoWeight(_smallBaggageCount, _largeBaggageCount);
            var cargoVolume = CalculateCargoVolume(_smallBaggageCount, _largeBaggageCount);

            ArgumentNullException.ThrowIfNull(flight.Aircraft);

            FlightReserver.ReserveTicket(flight.Aircraft, cargoWeight, cargoVolume, _seats);
        }

        private static double CalculateCargoWeight(
            uint smallBaggageCount, uint largeBaggageCount,
            double smallBaggageWeight = 15, double largeBaggageWeight = 30)
            => (smallBaggageCount * smallBaggageWeight) + (largeBaggageCount * largeBaggageWeight);

        private static double CalculateCargoVolume(
             uint smallBaggageCount, uint largeBaggageCount,
             double smallBaggageVolume = 0.045, double largeBaggageVolume = 0.090)
             => (smallBaggageCount * smallBaggageVolume) + (largeBaggageCount * largeBaggageVolume);
    }
}