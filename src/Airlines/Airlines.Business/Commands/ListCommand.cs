﻿using Airlines.Business.Commands.Utils;
using Airlines.DataContracts.Enums;
using Airlines.DataContracts.Extensions;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airports;

namespace Airlines.Business.Commands
{
    public class ListCommand : ICommand
    {
        private readonly string _inputData;
        private readonly string _from;
        private readonly IAirportRepository<Airport> _airportRepository;

        private ListCommand(string inputData, string from, IAirportRepository<Airport> airportRepository)
        {
            _inputData = inputData;
            _from = from;
            _airportRepository = airportRepository;
        }

        public bool ContinueProgramAfterExecution => true;

        public string? Output { get; private set; }

        public static ListCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var inputData = GetInputDataFromInputParts(inputParts);

            var from = GetFromValueFromInputParts(inputParts);
            if (!IsFromValueValid(from))
            {
                throw new ArgumentException("'from' is NOT valid!");
            }

            return new ListCommand(inputData, from, repositoryContainer.Airports);
        }

        private static bool IsFromValueValid(string fromValue)
            => fromValue is (nameof(Airport.City)) or (nameof(Airport.Country));

        private static string GetInputDataFromInputParts(string[] inputParts, int index = 1)
          => string.Join(" ", inputParts.TakeInRange(index, inputParts.Length - 2));

        private static string GetFromValueFromInputParts(string[] inputParts)
          => inputParts[^1];

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            IEnumerable<Airport>? airports = null;
            airports = _from switch
            {
                nameof(Airport.City) => _airportRepository.AllWithCityName(_inputData),
                nameof(Airport.Country) => _airportRepository.AllWithCountryName(_inputData),
                _ => throw new ArgumentException("'from' is NOT valid!"),
            };

            return airports == null || !airports.Any()
                    ? $"{nameof(Airport)}s with such '{_from}' doesn't exist!"
                    : OutputFormater.FormatBusinessDomainObjectsOutput(BusinessDomainObjectsTypes.Airport, airports);
        }
    }
}
