﻿namespace Airlines.Business.Commands
{
    public interface ICommand
    {
        public string? Output { get; }

        public bool ContinueProgramAfterExecution { get; }

        public void Execute();
    }
}
