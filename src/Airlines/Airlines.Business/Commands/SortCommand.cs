﻿using Airlines.DataContracts.Enums;
using Airlines.DataContracts.Extensions;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Business.Commands.Utils;
using Airlines.Persistence.Basic.Repository.Flights;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Airlines;

namespace Airlines.Business.Commands
{
    public class SortCommand : ICommand
    {
        private static readonly HashSet<BusinessDomainObjectsTypes> _supportedTypes = [
            BusinessDomainObjectsTypes.Airport,
            BusinessDomainObjectsTypes.Airline,
            BusinessDomainObjectsTypes.Flight
        ];

        private readonly BusinessDomainObjectsTypes _type;
        private readonly bool _orderAscending;

        private readonly IFlightRepository<Flight> _flightRepository;
        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly IAirlineRepository<Airline> _airlineRepository;

        private SortCommand(BusinessDomainObjectsTypes type,
            bool orderAscending,
            IFlightRepository<Flight> flightRepository,
            IAirportRepository<Airport> airportRepository,
            IAirlineRepository<Airline> airlineRepository)
        {
            _type = type;
            _orderAscending = orderAscending;
            _flightRepository = flightRepository;
            _airportRepository = airportRepository;
            _airlineRepository = airlineRepository;
        }

        public bool ContinueProgramAfterExecution => true;

        public string? Output { get; private set; }

        public static SortCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var businessDomainObjectTypeText = GetBusinessDomainObjectTypeFromInputParts(inputParts);
            var businessDomainObjectType = businessDomainObjectTypeText.ParseToEnum<BusinessDomainObjectsTypes>()
                ?? throw new ArgumentException("The given 'input data' is unknown!");

            if (!_supportedTypes.Contains(businessDomainObjectType))
            {
                throw new ArgumentException("This type is not supported!");
            }

            var orderTypeText = GetOrderTypeFromInputParts(inputParts);
            var orderAscending = true;
            if (orderTypeText != null)
            {
                orderAscending = orderTypeText == "ascending";
            }

            return new SortCommand(businessDomainObjectType,
                orderAscending,
                repositoryContainer.Flights,
                repositoryContainer.Airports,
                repositoryContainer.Airlines);
        }

        private static string GetBusinessDomainObjectTypeFromInputParts(string[] inputParts, int index = 1)
        {
            var typeText = inputParts[index];

            if (typeText.EndsWith('s'))
            {
                typeText = typeText.Remove(inputParts[index].Length - 1);
            }

            return typeText;
        }

        private static string? GetOrderTypeFromInputParts(string[] inputParts, int index = 2)
            => inputParts.Length > index + 1 ? inputParts[index] : null;

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            IEnumerable<BusinessDomainObject>? sortedCollection = null;
            if (_type == BusinessDomainObjectsTypes.Airport)
            {
                sortedCollection = _airportRepository.Sort(_orderAscending);
            }
            else if (_type == BusinessDomainObjectsTypes.Airline)
            {
                sortedCollection = _airlineRepository.Sort(_orderAscending);
            }
            else if (_type == BusinessDomainObjectsTypes.Flight)
            {
                sortedCollection = _flightRepository.Sort(_orderAscending);
            }

            ArgumentNullException.ThrowIfNull(sortedCollection);

            return OutputFormater.FormatBusinessDomainObjectsOutput(_type, sortedCollection);
        }
    }
}
