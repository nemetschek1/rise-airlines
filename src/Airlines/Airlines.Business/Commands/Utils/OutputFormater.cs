﻿using System.Text;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Commands.Utils
{
    public static class OutputFormater
    {
        public static string FormatBusinessDomainObjectsOutput(
            BusinessDomainObjectsTypes type,
            IEnumerable<BusinessDomainObject> collection)
        {
            var text = string.Join(Environment.NewLine, collection);

            var outputBuilder = new StringBuilder();
            outputBuilder.AppendLine($"{type}s:");
            outputBuilder.AppendLine(text);

            return outputBuilder.ToString().Trim();
        }

        public static string FormatErrorForUser(string message)
            => $"Error: {message}";
    }
}