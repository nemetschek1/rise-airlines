﻿using Airlines.Business.Route;
using Airlines.Business.Commands.Batch;
using Airlines.Business.Commands.Reserve;
using Airlines.Business.Commands.Route;
using Airlines.DataContracts.Enums;
using Airlines.DataContracts.Extensions;
using Airlines.Persistence.Basic.Container;

namespace Airlines.Business.Commands
{
    public static class CommandFactory
    {
        private static readonly HashSet<string> _commandsThatRequireSecondWordOfInput = ["route", "reserve", "batch"];

        public static ICommand Create(string[] inputParts,
            RepositoryContainer repositoryContainer,
            RouteGraph route,
            BatchData batchData)
        {
            var commandTypeText = GetCommandTypeFromInputParts(inputParts);
            var type = commandTypeText.ParseToEnum<CommandsTypes>();

            return type switch
            {
                CommandsTypes.Search => SearchCommand.Create(inputParts, repositoryContainer),
                CommandsTypes.Sort => SortCommand.Create(inputParts, repositoryContainer),
                CommandsTypes.Exists => ExistsCommand.Create(inputParts, repositoryContainer),
                CommandsTypes.List => ListCommand.Create(inputParts, repositoryContainer),

                CommandsTypes.RouteNew => RouteNewCommand.Create(route),
                CommandsTypes.RouteAdd => RouteAddCommand.Create(inputParts, repositoryContainer, route),
                CommandsTypes.RouteRemove => RouteRemoveCommand.Create(inputParts, repositoryContainer, route),
                CommandsTypes.RoutePrint => RoutePrintCommand.Create(route),
                CommandsTypes.RouteFind => RouteFindCommand.Create(inputParts, repositoryContainer, route),
                CommandsTypes.RouteCheck => RouteCheckCommand.Create(inputParts, repositoryContainer, route),
                CommandsTypes.RouteSearch => RouteSearchCommand.Create(inputParts, repositoryContainer, route),

                CommandsTypes.ReserveCargo => ReserveCargoCommand.Create(inputParts, repositoryContainer),
                CommandsTypes.ReserveTicket => ReserveTicketCommand.Create(inputParts, repositoryContainer),

                CommandsTypes.BatchStart => BatchStartCommand.Create(batchData),
                CommandsTypes.BatchRun => BatchRunCommand.Create(batchData),
                CommandsTypes.BatchCancel => BatchCancelCommand.Create(batchData),

                _ => EndCommand.Create(repositoryContainer),
            };
        }

        public static string GetCommandTypeFromInputParts(string[] inputParts, int index = 0)
        {
            var commandTypeText = inputParts[index];
            if (_commandsThatRequireSecondWordOfInput.Contains(commandTypeText))
            {
                commandTypeText += inputParts[1];
            }

            return commandTypeText;
        }
    }
}