﻿using System.Text;
using Airlines.Business.Commands.Utils;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airlines;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Business.Commands
{
    public class EndCommand : ICommand
    {
        private readonly IFlightRepository<Flight> _flightRepository;
        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly IAirlineRepository<Airline> _airlineRepository;

        private EndCommand(IFlightRepository<Flight> flightRepository,
            IAirportRepository<Airport> airportRepository,
            IAirlineRepository<Airline> airlineRepository)
        {
            _flightRepository = flightRepository;
            _airportRepository = airportRepository;
            _airlineRepository = airlineRepository;
        }

        public bool ContinueProgramAfterExecution => false;

        public string? Output { get; private set; }

        public static EndCommand Create(RepositoryContainer repositoryContainer)
            => new(repositoryContainer.Flights, repositoryContainer.Airports, repositoryContainer.Airlines);

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            var outputBuilder = new StringBuilder();

            AddCollectionVisualization(_airportRepository.Sort(), BusinessDomainObjectsTypes.Airport, outputBuilder);
            AddCollectionVisualization(_airlineRepository.Sort(), BusinessDomainObjectsTypes.Airline, outputBuilder);
            AddCollectionVisualization(_flightRepository.Sort(), BusinessDomainObjectsTypes.Flight, outputBuilder);

            return outputBuilder.ToString();
        }

        private static void AddCollectionVisualization(IEnumerable<BusinessDomainObject> collection,
            BusinessDomainObjectsTypes type,
            StringBuilder stringBuilder)
        {
            var resultOfPrint = OutputFormater.FormatBusinessDomainObjectsOutput(type, collection);
            stringBuilder.AppendLine(resultOfPrint);
        }
    }
}
