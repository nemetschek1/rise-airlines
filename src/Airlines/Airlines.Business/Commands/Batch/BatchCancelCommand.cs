﻿namespace Airlines.Business.Commands.Batch
{
    public class BatchCancelCommand : ICommand
    {
        private readonly BatchData _batchData;

        private BatchCancelCommand(BatchData batchData)
            => _batchData = batchData;

        public string? Output => string.Empty;

        public bool ContinueProgramAfterExecution => true;

        public static BatchCancelCommand Create(BatchData batchData)
            => new(batchData);

        public void Execute()
        {
            _batchData.IsBatchModeActive = false;
            _batchData.CleanCommands();
        }
    }
}
