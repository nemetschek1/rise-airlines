﻿namespace Airlines.Business.Commands.Batch
{
    public class BatchStartCommand : ICommand
    {
        private readonly BatchData _batchData;

        private BatchStartCommand(BatchData batchData)
            => _batchData = batchData;

        public string? Output => string.Empty;

        public bool ContinueProgramAfterExecution => true;

        public static BatchStartCommand Create(BatchData batchData)
               => new(batchData);

        public void Execute()
        {
            _batchData.IsBatchModeActive = true;
            _batchData.CleanCommands();
        }
    }
}