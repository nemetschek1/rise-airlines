﻿namespace Airlines.Business.Commands.Batch
{
    public class BatchRunCommand : ICommand
    {
        private readonly BatchData _batchData;

        private BatchRunCommand(BatchData batchData)
            => _batchData = batchData;

        public string? Output => string.Empty;

        public bool ContinueProgramAfterExecution => true;

        public static BatchRunCommand Create(BatchData batchData)
            => new(batchData);

        public void Execute()
        {
            if (_batchData.IsBatchModeActive)
            {
                _batchData.Commands();
                _batchData.IsBatchModeActive = false;
            }
        }
    }
}