﻿namespace Airlines.Business.Commands.Batch
{
    public class BatchData
    {
        public BatchData()
        {
            IsBatchModeActive = false;
            CleanCommands();
        }

        public bool IsBatchModeActive { get; set; }

        public Action Commands { get; set; }

        public void AddCommand(Action action) => Commands += action;

        public void CleanCommands() => Commands = DoNothingFunc;

        private static void DoNothingFunc() { }
    }
}
