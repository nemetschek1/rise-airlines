﻿using Airlines.Business.Route;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airports;
using System.Text;

namespace Airlines.Business.Commands.Route
{
    public class RouteFindCommand : ICommand
    {
        private readonly string _startAirportName;
        private readonly string _destinationAirportName;

        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly RouteGraph _route;

        private RouteFindCommand(string startAirportName,
            string destinationAirportName,
            IAirportRepository<Airport> airportRepository,
            RouteGraph route)
        {
            _startAirportName = startAirportName;
            _destinationAirportName = destinationAirportName;
            _airportRepository = airportRepository;
            _route = route;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static RouteFindCommand Create(string[] inputParts, RepositoryContainer repositoryContainer, RouteGraph route)
        {
            var startAirportName = RouteUtils.GetStartAirportFromInputParts(inputParts);
            var destinationAirportName = RouteUtils.GetDestinationAirportFromInputParts(inputParts);

            return new RouteFindCommand(startAirportName, destinationAirportName, repositoryContainer.Airports, route);
        }

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            var startAirport = _airportRepository.FindByKey(_startAirportName);
            var destinationAirport = _airportRepository.FindByKey(_destinationAirportName);

            var routesToAirport = _route.FindAllRoutesToAirport(startAirport, destinationAirport);

            var outputBuilder = new StringBuilder();

            outputBuilder.AppendLine("Routes to searched airport:");
            foreach (var route in routesToAirport)
            {
                outputBuilder.AppendLine(RouteUtils.GenerateRouteDisplayText(route));
            }

            return outputBuilder.ToString().Trim();
        }
    }
}
