﻿using Airlines.Business.Route;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airports;

namespace Airlines.Business.Commands.Route
{
    public class RouteCheckCommand : ICommand
    {
        private readonly string _startAirportName;
        private readonly string _destinationAirportName;
        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly RouteGraph _route;

        private RouteCheckCommand(string startAirportName,
            string destinationAirportName,
            IAirportRepository<Airport> airportRepository,
            RouteGraph route)
        {
            _startAirportName = startAirportName;
            _destinationAirportName = destinationAirportName;
            _airportRepository = airportRepository;
            _route = route;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static RouteCheckCommand Create(string[] inputParts, RepositoryContainer repositoryContainer, RouteGraph route)
        {
            var startAirportName = RouteUtils.GetStartAirportFromInputParts(inputParts);
            var destinationAirportName = RouteUtils.GetDestinationAirportFromInputParts(inputParts);

            return new RouteCheckCommand(startAirportName, destinationAirportName, repositoryContainer.Airports, route);
        }

        public void Execute()
        {
            var startAirport = _airportRepository.FindByKey(_startAirportName);
            var destinationAirport = _airportRepository.FindByKey(_destinationAirportName);

            var exists = _route.DoesRouteToAirportExist(startAirport, destinationAirport);

            Output = GenerateOutput(exists);
        }

        private static string GenerateOutput(bool exists)
            => exists ? "Route exists" : "Route does NOT exist";
    }
}
