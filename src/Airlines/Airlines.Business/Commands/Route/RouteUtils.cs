﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Commands.Route
{
    public class RouteUtils
    {
        public static string GetStartAirportFromInputParts(string[] inputParts, int startIndex = 2)
        {
            var endIndex = Array.IndexOf(inputParts, "->");

            return string.Join(" ", inputParts[startIndex..endIndex]);
        }

        public static string GetDestinationAirportFromInputParts(string[] inputParts, int endWithout = 0)
        {
            var startIndex = Array.IndexOf(inputParts, "->") + 1;

            return string.Join(" ", inputParts[startIndex..(inputParts.Length - endWithout)]);
        }

        public static string GenerateRouteDisplayText(List<Airport> route)
            => string.Join(" -> ", route.Select(x => x.Name));
    }
}
