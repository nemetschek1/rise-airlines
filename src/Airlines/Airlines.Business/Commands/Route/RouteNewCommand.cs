﻿using Airlines.Business.Route;
namespace Airlines.Business.Commands.Route
{
    public class RouteNewCommand : ICommand
    {
        private readonly RouteGraph _route;

        private RouteNewCommand(RouteGraph route) => _route = route;

        public string? Output => string.Empty;

        public bool ContinueProgramAfterExecution => true;

        public static RouteNewCommand Create(RouteGraph route)
            => new(route);

        public void Execute() => _route.Clear();
    }
}