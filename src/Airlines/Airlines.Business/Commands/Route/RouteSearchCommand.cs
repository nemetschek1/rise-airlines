﻿using Airlines.Business.Route.RouteDeterminer;
using System.Text;
using Airlines.Business.Route;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Business.Commands.Route
{
    public class RouteSearchCommand : ICommand
    {
        private readonly string _startAirportName;
        private readonly string _destinationAirportName;
        private readonly string _searchStrategyText;

        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly RouteGraph _route;

        private RouteSearchCommand(string startAirportName,
            string destinationAirportName,
            string searchStrategyText,
            IAirportRepository<Airport> airportRepository,
            RouteGraph route)
        {
            _startAirportName = startAirportName;
            _destinationAirportName = destinationAirportName;
            _searchStrategyText = searchStrategyText;
            _airportRepository = airportRepository;
            _route = route;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static RouteSearchCommand Create(string[] inputParts, RepositoryContainer repositoryContainer, RouteGraph route)
        {
            var startAirportName = RouteUtils.GetStartAirportFromInputParts(inputParts);
            var destinationAirportName = RouteUtils.GetDestinationAirportFromInputParts(inputParts, 1);
            var searchStrategyText = GetStrategyFromInputParts(inputParts);

            return new RouteSearchCommand(startAirportName,
                destinationAirportName,
                searchStrategyText,
                repositoryContainer.Airports,
                route);
        }

        private static string GetStrategyFromInputParts(string[] inputParts)
            => inputParts[^1];

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            var startAirport = _airportRepository.FindByKey(_startAirportName);
            var destinationAirport = _airportRepository.FindByKey(_destinationAirportName);

            var routeDeterminer = RouteDeterminerFactory.Create(_searchStrategyText);

            var shortestRouteToAirport = _route
                .FindShortestRouteToAirport(startAirport, destinationAirport, routeDeterminer);

            var outputBuilder = new StringBuilder();

            if (shortestRouteToAirport == null)
            {
                outputBuilder.AppendLine("No route to airport exists");
            }
            else
            {
                outputBuilder.AppendLine("Shortest route to airport:");
                outputBuilder.AppendLine(RouteUtils.GenerateRouteDisplayText(shortestRouteToAirport));
            }

            return outputBuilder.ToString().Trim();
        }
    }
}
