﻿using Airlines.Business.Route;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Flights;
using Airlines.Persistence.Basic.Validation;

namespace Airlines.Business.Commands.Route
{
    public class RouteAddCommand : ICommand
    {
        private readonly string _flightIdentifier;
        private readonly IFlightRepository<Flight> _flightRepository;
        private readonly RouteGraph _route;

        private RouteAddCommand(string flightIdentifier, IFlightRepository<Flight> flightRepository, RouteGraph route)
        {
            _flightIdentifier = flightIdentifier;
            _flightRepository = flightRepository;
            _route = route;
        }

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static RouteAddCommand Create(string[] inputParts,
            RepositoryContainer repositoryContainer,
            RouteGraph route)
        {
            var flightIdentifier = GetFlightIdentifierFromInputParts(inputParts);

            return new RouteAddCommand(flightIdentifier, repositoryContainer.Flights, route);
        }

        private static string GetFlightIdentifierFromInputParts(string[] inputParts, int index = 2)
           => inputParts[index];

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            var flight = _flightRepository.FindByKey(_flightIdentifier);

            RouteValidationHandler.Validate(flight);

            _route.Add(flight!);

            return "Successfully added new flight to route!";
        }
    }
}