﻿using Airlines.Business.Route;

namespace Airlines.Business.Commands.Route
{
    public class RoutePrintCommand : ICommand
    {
        private readonly RouteGraph _route;

        private RoutePrintCommand(RouteGraph route) => _route = route;

        public string? Output { get; private set; }

        public bool ContinueProgramAfterExecution => true;

        public static RoutePrintCommand Create(RouteGraph route)
            => new(route);

        public void Execute() => Output = _route?.ToString();
    }
}