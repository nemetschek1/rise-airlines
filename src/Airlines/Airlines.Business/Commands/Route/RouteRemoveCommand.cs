﻿using Airlines.Business.Route;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Business.Commands.Route
{
    public class RouteRemoveCommand : ICommand
    {
        private readonly string _flightIdentifier;
        private readonly IFlightRepository<Flight> _flightRepository;
        private readonly RouteGraph _route;

        private RouteRemoveCommand(string flightIdentifier, IFlightRepository<Flight> flightRepository, RouteGraph route)
        {
            _flightIdentifier = flightIdentifier;
            _flightRepository = flightRepository;
            _route = route;
        }

        public string? Output => string.Empty;

        public bool ContinueProgramAfterExecution => true;

        public static RouteRemoveCommand Create(string[] inputParts, RepositoryContainer repositoryContainer, RouteGraph route)
        {
            var flightIdentifier = GetFlightIdentifierFromInputParts(inputParts);

            return new RouteRemoveCommand(flightIdentifier, repositoryContainer.Flights, route);
        }

        private static string GetFlightIdentifierFromInputParts(string[] inputParts, int index = 2)
            => inputParts[index];

        public void Execute()
        {
            var flight = _flightRepository.FindByKey(_flightIdentifier);

            ArgumentNullException.ThrowIfNull(flight);

            _route.Remove(flight);
        }
    }
}