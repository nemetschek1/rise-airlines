﻿using Airlines.Business.Algorithms.Search;
using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airlines;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Business.Commands
{
    public class SearchCommand : ICommand
    {
        private readonly SearchTerm _searchTerm;

        private readonly IFlightRepository<Flight> _flightRepository;
        private readonly IAirportRepository<Airport> _airportRepository;
        private readonly IAirlineRepository<Airline> _airlineRepository;

        private SearchCommand(SearchTerm searchTerm,
            IFlightRepository<Flight> flightRepository,
            IAirportRepository<Airport> airportRepository,
            IAirlineRepository<Airline> airlineRepository)
        {
            _searchTerm = searchTerm;
            _flightRepository = flightRepository;
            _airportRepository = airportRepository;
            _airlineRepository = airlineRepository;
        }

        public bool ContinueProgramAfterExecution => true;

        public string? Output { get; private set; }

        public static SearchCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var searchTermText = GetSearchTermFromInputParts(inputParts);
            var searchTerm = new SearchTerm(searchTermText);

            return new SearchCommand(searchTerm,
                repositoryContainer.Flights,
                repositoryContainer.Airports,
                repositoryContainer.Airlines);
        }

        private static string GetSearchTermFromInputParts(string[] inputParts, int index = 1)
            => inputParts[index];

        public void Execute() => Output ??= GenerateOutput();

        private string GenerateOutput()
        {
            string? output = null;

            var successMessage = "The 'search term' was found in {0}s.";
            if (CheckIfRepositoryContainsSearchTerm(_airportRepository.All()))
            {
                output = string.Format(successMessage, nameof(Airport));
            }
            else if (CheckIfRepositoryContainsSearchTerm(_airlineRepository.All()))
            {
                output = string.Format(successMessage, nameof(Airline));
            }
            else if (CheckIfRepositoryContainsSearchTerm(_flightRepository.All()))
            {
                output = string.Format(successMessage, nameof(Flight));
            }

            return output ?? "The 'search term' was NOT found!";
        }

        private bool CheckIfRepositoryContainsSearchTerm(IEnumerable<BusinessDomainObject> all)
            => all.Exists(_searchTerm, SearchTypes.Binary);
    }
}
