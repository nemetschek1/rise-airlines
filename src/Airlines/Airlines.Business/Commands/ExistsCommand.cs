﻿using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository.Airlines;

namespace Airlines.Business.Commands
{
    public class ExistsCommand : ICommand
    {
        private readonly string _airlineName;
        private readonly IAirlineRepository<Airline> _airlineRepository;

        private ExistsCommand(string airlineName, IAirlineRepository<Airline> airlineRepository)
        {
            _airlineName = airlineName;
            _airlineRepository = airlineRepository;
        }

        public bool ContinueProgramAfterExecution => true;

        public string? Output { get; private set; }

        public static ExistsCommand Create(string[] inputParts, RepositoryContainer repositoryContainer)
        {
            var airlineName = GetAirlineNameFromInputParts(inputParts);

            return new ExistsCommand(airlineName, repositoryContainer.Airlines);
        }

        private static string GetAirlineNameFromInputParts(string[] inputParts, int index = 1)
          => inputParts[index];

        public void Execute() => Output = GenerateOutput();

        private string GenerateOutput()
        {
            var exists = _airlineRepository.FindByKey(_airlineName) != null;

            return exists.ToString();
        }
    }
}
