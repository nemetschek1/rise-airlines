﻿using Airlines.DataContracts.Enums;

namespace Airlines.Business.Algorithms.Search
{
    public static class BinarySearch
    {
        public static bool Find<T>(IEnumerable<T> collection, T searchedItem)
                where T : IComparable<T>
        {
            var array = collection.OrderBy(x => x).ToArray();

            var left = 0;
            var right = array.Length - 1;

            while (left <= right)
            {
                var mid = left + ((right - left) / 2);

                var comparingResult = array[mid].CompareTo(searchedItem);
                if (comparingResult == (int)ComparingTypes.Equal)
                {
                    return true;
                }

                if (comparingResult == (int)ComparingTypes.Smaller)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            return false;
        }
    }
}