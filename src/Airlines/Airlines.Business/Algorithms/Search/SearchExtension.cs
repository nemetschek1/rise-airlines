﻿using Airlines.DataContracts.Enums;

namespace Airlines.Business.Algorithms.Search
{
    public static class SearchExtension
    {
        public static bool Exists<T>(this IEnumerable<T> collection, T searchedItem, SearchTypes? type = null)
            where T : IComparable<T>
        {
            return type switch
            {
                SearchTypes.Binary => BinarySearch.Find(collection, searchedItem),
                SearchTypes.Linear => LinearSearch.Find(collection, searchedItem),
                _ => collection.Contains(searchedItem),
            };
        }
    }
}