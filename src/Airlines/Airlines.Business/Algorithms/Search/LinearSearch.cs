﻿using Airlines.DataContracts.Enums;

namespace Airlines.Business.Algorithms.Search
{
    public static class LinearSearch
    {
        public static bool Find<T>(IEnumerable<T> collection, T searchedItem)
                where T : IComparable<T>
        {
            foreach (var item in collection)
            {
                if (item.CompareTo(searchedItem) == (int)ComparingTypes.Equal)
                {
                    return true;
                }
            }

            return false;
        }
    }
}