﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository.Airlines;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Services.Airlines
{
    public class AirlineService(IAirlineRepository<Airline> airlineRepository) : IAirlineService
    {
        private readonly IAirlineRepository<Airline> _airlineRepository = airlineRepository;

        public IEnumerable<Airline> GetAll(SearchInputModel? searchInputModel = null)
        {
            var all = _airlineRepository.All();

            all = SearchingService.FilterByKeyword(all, searchInputModel);

            return all.ToList();
        }

        public int GetCountOfAll() => _airlineRepository.Count;

        public bool Add(Airline airline)
        {
            Validate(airline);

            return _airlineRepository.Add(airline);
        }

        private static void Validate(Airline airline)
        {
            if (string.IsNullOrWhiteSpace(airline.Name) || airline.Name.Length > 256)
            {
                throw new ValidationException("Airline name is invalid!");
            }

            if (airline.FleetSize < 0)
            {
                throw new ValidationException("Airline fleetsize is invalid!");
            }

            if (airline.Founded < new DateOnly(1909, 11, 16))
            {
                throw new ValidationException("Airline founded date is invalid!");
            }

            if (!(airline.Description == null
                || (!string.IsNullOrWhiteSpace(airline.Description) && airline.Description.Length < 256)))
            {
                throw new ValidationException("Airline description is invalid!");
            }
        }

        public Airline? GetOneByKey(int id) => _airlineRepository.FindByKey(id.ToString());

        public bool Update(Airline airline) => _airlineRepository.Update(airline);

        public bool Delete(int id) => _airlineRepository.Delete(id.ToString());
    }
}
