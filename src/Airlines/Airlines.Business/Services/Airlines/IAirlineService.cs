﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Services.Airlines
{
    public interface IAirlineService
    {
        public int GetCountOfAll();

        public IEnumerable<Airline> GetAll(SearchInputModel? searchInputModel = null);

        public Airline? GetOneByKey(int id);

        public bool Add(Airline airline);

        public bool Update(Airline airline);

        public bool Delete(int id);
    }
}