﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Services.Airports
{
    public interface IAirportService
    {
        public int GetCountOfAll();

        public IEnumerable<Airport> GetAll(SearchInputModel? searchInputModel = null);

        public Airport? GetOneByKey(int id);

        public bool Add(Airport airport);

        public bool Update(Airport airport);

        public bool Delete(int id);
    }
}