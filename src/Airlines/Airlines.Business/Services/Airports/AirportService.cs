﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository.Airports;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Services.Airports
{
    public class AirportService(IAirportRepository<Airport> airportRepository) : IAirportService
    {
        private readonly IAirportRepository<Airport> _airportRepository = airportRepository;

        public IEnumerable<Airport> GetAll(SearchInputModel? searchInputModel = null)
        {
            var all = _airportRepository.All();

            all = SearchingService.FilterByKeyword(all, searchInputModel);

            return all.ToList();
        }

        public int GetCountOfAll() => _airportRepository.Count;

        public bool Add(Airport airport)
        {
            Validate(airport);

            return _airportRepository.Add(airport);
        }

        private static void Validate(Airport airport)
        {
            if (string.IsNullOrWhiteSpace(airport.Name) || airport.Name.Length > 256)
            {
                throw new ValidationException("Airport name is invalid!");
            }

            if (string.IsNullOrWhiteSpace(airport.Country) || airport.Country.Length > 256)
            {
                throw new ValidationException("Airport country is invalid!");
            }

            if (string.IsNullOrWhiteSpace(airport.City) || airport.City.Length > 256)
            {
                throw new ValidationException("Airport city is invalid!");
            }

            if (string.IsNullOrWhiteSpace(airport.Code) || airport.Code.Length > 4)
            {
                throw new ValidationException("Airport code is invalid!");
            }

            if (airport.RunwaysCount < 0)
            {
                throw new ValidationException("Airport runways count is invalid!");
            }

            if (airport.Founded < new DateOnly(1976, 1, 1))
            {
                throw new ValidationException("Airport founded is invalid!");
            }
        }

        public Airport? GetOneByKey(int id) => _airportRepository.FindByKey(id.ToString());

        public bool Update(Airport airport) => _airportRepository.Update(airport);

        public bool Delete(int id) => _airportRepository.Delete(id.ToString());
    }
}
