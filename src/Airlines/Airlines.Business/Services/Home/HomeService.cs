﻿using Airlines.Business.Models;
using Airlines.Business.Services.Airlines;
using Airlines.Business.Services.Airports;
using Airlines.Business.Services.Flights;

namespace Airlines.Business.Services.Home
{
    public class HomeService(IAirlineService airlineService,
        IAirportService airportService,
        IFlightService flightService) : IHomeService
    {
        private readonly IAirlineService _airlineService = airlineService;
        private readonly IAirportService _airportService = airportService;
        private readonly IFlightService _flightService = flightService;

        public HomeIndexPageViewModel GetHomeIndexPageViewModel()
        {
            var output = new HomeIndexPageViewModel
            {
                AirlinesCount = _airlineService.GetCountOfAll(),
                AirportsCount = _airportService.GetCountOfAll(),
                FlightsCount = _flightService.GetCountOfAll()
            };

            return output;
        }
    }
}