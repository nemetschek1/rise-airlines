﻿using Airlines.Business.Models;

namespace Airlines.Business.Services.Home
{
    public interface IHomeService
    {
        public HomeIndexPageViewModel GetHomeIndexPageViewModel();
    }
}