﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Services.Flights
{
    public interface IFlightService
    {
        public int GetCountOfAll();

        public IEnumerable<Flight> GetAll(SearchInputModel? searchInputModel = null);

        public Flight? GetOneByKey(int id);

        public bool Add(Flight flight);

        public bool Update(Flight flight);

        public bool Delete(int id);
    }
}