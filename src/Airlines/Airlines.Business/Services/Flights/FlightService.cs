﻿using Airlines.Business.Models;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository.Flights;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Services.Flights
{
    public class FlightService(IFlightRepository<Flight> flightRepository) : IFlightService
    {
        private readonly IFlightRepository<Flight> _flightRepository = flightRepository;

        public IEnumerable<Flight> GetAll(SearchInputModel? searchInputModel = null)
        {
            var all = _flightRepository.All();

            all = SearchingService.FilterByKeyword(all, searchInputModel);

            return all.ToList();
        }

        public int GetCountOfAll() => _flightRepository.Count;

        public bool Add(Flight flight)
        {
            Validate(flight);

            return _flightRepository.Add(flight);
        }

        private static void Validate(Flight flight)
        {
            if (string.IsNullOrWhiteSpace(flight.Number) || flight.Number.Length > 256)
            {
                throw new ValidationException("Flight number is invalid!");
            }

            if (string.IsNullOrWhiteSpace(flight.From) || flight.From.Length > 4)
            {
                throw new ValidationException("Flight 'from' is invalid!");
            }

            if (string.IsNullOrWhiteSpace(flight.To) || flight.To.Length > 4)
            {
                throw new ValidationException("Flight 'to' is invalid!");
            }

            if (flight.Departure > flight.Arrival)
            {
                throw new ValidationException("Flight departure must be less than arrival!");
            }
        }

        public Flight? GetOneByKey(int id) => _flightRepository.FindByKey(id.ToString());

        public bool Update(Flight flight) => _flightRepository.Update(flight);

        public bool Delete(int id) => _flightRepository.Delete(id.ToString());
    }
}
