﻿using Airlines.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Business.Services
{
    public class SearchingService
    {
        public static IEnumerable<T> FilterByKeyword<T>(IEnumerable<T> all, SearchInputModel? searchInputModel)
        {
            if (searchInputModel == null
                    || string.IsNullOrEmpty(searchInputModel.Category)
                    || string.IsNullOrEmpty(searchInputModel.Keyword))
            {
                return all;
            }

            return ((IQueryable<T>)all).Where(x => EF.Property<object>(x!, searchInputModel.Category).ToString()
                !.Contains(searchInputModel.Keyword));
        }
    }
}
