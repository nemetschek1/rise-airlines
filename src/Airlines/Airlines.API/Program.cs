using Airlines.API.Infrastructure.ExceptionHandler;
using Airlines.Business.MappingProfiles;
using Airlines.Business.Services.Airports;
using Airlines.Business.Services.Airlines;
using Airlines.Business.Services.Flights;
using Airlines.Business.Services.Home;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository.Airlines;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            ConfigureServices(builder.Services, builder.Configuration);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseExceptionHandlerMiddleware();

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }

        private static void ConfigureServices(IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton(_ => CreateMapper());

            services.AddTransient<IAirportRepository<Airport>, WebDbAirportRepository>();
            services.AddTransient<IAirlineRepository<Airline>, WebDbAirlineRepository>();
            services.AddTransient<IFlightRepository<Flight>, WebDbFlightRepository>();

            services.AddTransient<IAirportService, AirportService>();
            services.AddTransient<IAirlineService, AirlineService>();
            services.AddTransient<IFlightService, FlightService>();
            services.AddTransient<IHomeService, HomeService>();
        }

        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AirportProfile>();
                cfg.AddProfile<AirlineProfile>();
                cfg.AddProfile<FlightProfile>();
            });

            return new Mapper(config);
        }
    }
}