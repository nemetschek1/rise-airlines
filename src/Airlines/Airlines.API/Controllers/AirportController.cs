using Airlines.Business.Models;
using Airlines.Business.Services.Airports;
using Airlines.Persistence.Basic.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AirportController(IAirportService airportService, IMapper mapper) : BaseController
    {
        private readonly IAirportService _airportService = airportService;
        private readonly IMapper _mapper = mapper;

        [HttpGet("{id}")]
        public IActionResult GetOne(int id)
        {
            var airport = _airportService.GetOneByKey(id);
            if (airport == null)
            {
                return NotFound();
            }

            return Ok(airport);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var airports = _airportService.GetAll();
            if (airports == null)
            {
                return NotFound();
            }

            return Ok(airports);
        }

        [HttpPost]
        public IActionResult Create(AirportAddDTO airportDTO)
        {
            var airport = _mapper.Map<Airport>(airportDTO);

            if (!_airportService.Add(airport))
            {
                return InternalServerError();
            }

            return Created();
        }

        [HttpPut]
        public IActionResult Edit(AirportUpdateDTO airportDTO)
        {
            var airport = _mapper.Map<Airport>(airportDTO);

            if (!_airportService.Update(airport))
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_airportService.Delete(id))
            {
                return InternalServerError();
            }

            return Ok();
        }
    }
}