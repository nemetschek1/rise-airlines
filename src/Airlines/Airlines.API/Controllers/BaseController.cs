﻿using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    public class BaseController : ControllerBase
    {
        protected IActionResult InternalServerError() 
            => StatusCode(500, "Internal Server Error: Something went wrong.");
    }
}
