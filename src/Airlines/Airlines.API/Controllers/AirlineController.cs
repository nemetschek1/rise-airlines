using Airlines.Business.Models;
using Airlines.Business.Services.Airlines;
using Airlines.Persistence.Basic.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AirlineController(IAirlineService airlineService, IMapper mapper) : BaseController
    {
        private readonly IAirlineService _airlineService = airlineService;
        private readonly IMapper _mapper = mapper;

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var airline = _airlineService.GetOneByKey(id);
            if (airline == null)
            {
                return NotFound();
            }

            return Ok(airline);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var airlines = _airlineService.GetAll();
            if (airlines == null)
            {
                return NotFound();
            }

            return Ok(airlines);
        }

        [HttpPost]
        public IActionResult Create(AirlineAddDTO airlineDTO)
        {
            var airline = _mapper.Map<Airline>(airlineDTO);

            if (!_airlineService.Add(airline))
            {
                return InternalServerError();
            }

            return Created();
        }

        [HttpPut]
        public IActionResult Edit(AirlineUpdateDTO airlineDTO)
        {
            var airline = _mapper.Map<Airline>(airlineDTO);

            if (!_airlineService.Update(airline))
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_airlineService.Delete(id))
            {
                return InternalServerError();
            }

            return Ok();
        }
    }
}