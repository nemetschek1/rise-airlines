using AutoMapper;
using Airlines.Business.Models;
using Airlines.Business.Services.Flights;
using Airlines.Persistence.Basic.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FlightController(IFlightService flightService, IMapper mapper) : BaseController
    {
        private readonly IFlightService _flightService = flightService;
        private readonly IMapper _mapper = mapper;

        [HttpGet("{id}")]
        public IActionResult GetOne(int id)
        {
            var flight = _flightService.GetOneByKey(id);
            if (flight == null)
            {
                return NotFound();
            }

            return Ok(flight);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var flights = _flightService.GetAll();
            if (flights == null)
            {
                return NotFound();
            }

            return Ok(flights);
        }

        [HttpPost]
        public IActionResult Create(FlightAddDTO flightDTO)
        {
            var flight = _mapper.Map<Flight>(flightDTO);

            if (!_flightService.Add(flight))
            {
                return InternalServerError();
            }

            return Created();
        }

        [HttpPut]
        public IActionResult Edit(FlightUpdateDTO flightDTO)
        {
            var flight = _mapper.Map<Flight>(flightDTO);

            if (!_flightService.Update(flight))
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_flightService.Delete(id))
            {
                return InternalServerError();
            }

            return Ok();
        }
    }
}