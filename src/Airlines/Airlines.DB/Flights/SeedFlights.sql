﻿INSERT INTO Flights ([Number], [From], [To], [Departure], [Arrival])
	VALUES ('FL123', 'JFK', 'LAX', '2024-5-15 11:12:01', '2024-5-15 14:12:01'),
		('FL456', 'LAX', 'ORD', '2024-5-6 10:00:00', '2024-5-6 12:00:00'),
		('FL456', 'LAX', 'ORD', '2024-4-16 10:00:00', '2024-4-16 12:00:00');