SELECT TOP(1) *
	FROM Flights AS f
	ORDER BY DATEDIFF(MINUTE, f.Departure, f.Arrival) DESC;