SELECT f.[Number] AS FlightNumber,
		f.[Departure],
		aFrom.[Name] AS AirportFromName,
		f.[Arrival],
		aTo.[Name] AS AirportToName
	FROM Flights AS f
	JOIN Airports AS aTo ON aTo.[Code] = f.[To]
	JOIN Airports AS aFrom ON aFrom.[Code] = f.[From]
	WHERE CONVERT(DATE, f.[Departure]) = CONVERT(DATE, DATEADD(DAY, 1, GETDATE()));