-- Departure is less than now (15.04.2024)
INSERT INTO Flights ([Number], [From], [To], [Departure], [Arrival])
	VALUES ('FL123', 'JFK', 'LAX', '2024-3-15 11:12:01', '2024-5-15 14:12:01');

-- Arrival is less than now (15.04.2024)
-- Arrival is less than Departure
INSERT INTO Flights ([Number], [From], [To], [Departure], [Arrival])
	VALUES ('FL123', 'JFK', 'LAX', '2024-5-15 11:12:01', '2024-3-15 14:12:01');