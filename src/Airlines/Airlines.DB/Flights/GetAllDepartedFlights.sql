SELECT COUNT(f.Id) AS PastFlightsCount
	FROM Flights AS f
	WHERE CONVERT(DATE, f.Departure) < CONVERT(DATE, GETDATE());