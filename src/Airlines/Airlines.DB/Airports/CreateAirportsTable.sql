﻿CREATE TABLE Airports(
	[Name]			VARCHAR(256),
	[Country]		VARCHAR(256) NOT NULL,
	[City]			VARCHAR(256) NOT NULL,
	[Code]			VARCHAR(4) NOT NULL PRIMARY KEY,
	[RunwaysCount]	INT NOT NULL,
	[Founded]		DATE NOT NULL
);

CREATE NONCLUSTERED INDEX IX_Airports_City
	ON Airports (City);

CREATE NONCLUSTERED INDEX IX_Airports_Country
	ON Airports (Country);