﻿INSERT INTO Airports ([Name], [Country], [City], [Code], [RunwaysCount], [Founded])
	VALUES ('JF10', 'USA', 'New York', 'JFK', 2, '2006-1-29'),
		('JFK9', 'USA', 'New York', 'LAX', 1, '2012-10-2'),
		('JFK6', 'USA', 'New York', 'ORD', 4, '2010-10-2');