﻿namespace Airlines.Persistence.Basic.Entities
{
    public class Flight
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public DateTime Departure { get; set; }

        public DateTime Arrival { get; set; }

        public virtual Airport FromNavigation { get; set; }

        public virtual Airport ToNavigation { get; set; }
    }
}