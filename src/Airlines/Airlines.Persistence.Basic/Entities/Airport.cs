﻿namespace Airlines.Persistence.Basic.Entities
{
    public class Airport
    {
        public string? Name { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Code { get; set; }

        public int RunwaysCount { get; set; }

        public DateOnly Founded { get; set; }

        public virtual ICollection<Flight> FlightFromNavigations { get; set; } = [];

        public virtual ICollection<Flight> FlightToNavigations { get; set; } = [];
    }
}