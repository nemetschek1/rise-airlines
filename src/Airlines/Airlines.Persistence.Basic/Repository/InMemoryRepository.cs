﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository
{
    public abstract class InMemoryRepository<T> : IRepository<T>
    {
        protected readonly HashSet<T> _collection = [];

        public virtual int Count => _collection.Count;

        public virtual bool Add(T item)
        {
            if (Contains(item))
            {
                return false;
            }

            _collection.Add(item);

            return true;
        }

        public virtual IEnumerable<T> All() => _collection;

        public virtual bool Contains(T item) => _collection.Contains(item);

        public virtual T? FindByKey(string key)
        {
            var searchTerm = new SearchTerm(key);

            return _collection.FirstOrDefault(x => x?.Equals(searchTerm) ?? false);
        }

        public abstract IEnumerable<T> Sort(bool orderAscending = true);

        public bool Update(T item) => throw new NotImplementedException();

        public bool Delete(string key) => throw new NotImplementedException();
    }
}
