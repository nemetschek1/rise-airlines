﻿namespace Airlines.Persistence.Basic.Repository.RouteFlights
{
    public interface IRouteFlightRepository<T> : IRepository<T>
    {
        void Clear();

        void Remove(T flight);
    }
}
