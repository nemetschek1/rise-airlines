﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.RouteFlights
{
    public class InMemoryRouteFlightRepository : InMemoryRepository<Flight>, IRouteFlightRepository<Flight>
    {
        public void Clear() => _collection.Clear();

        public void Remove(Flight flight) => _collection.Remove(flight);

        public override IEnumerable<Flight> Sort(bool orderAscending = true)
            => throw new NotImplementedException();
    }
}
