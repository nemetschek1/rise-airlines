﻿using Airlines.Persistence.Basic.Algorithms.Sort;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.Airlines
{
    public class InMemoryAirlineRepository : InMemoryRepository<Airline>, IAirlineRepository<Airline>
    {
        private static readonly SortAlgorithm _sortingAlgorithm = new SelectionSort();

        public override IEnumerable<Airline> Sort(bool orderAscending = true)
            => _sortingAlgorithm.Execute(All(), orderAscending);
    }
}
