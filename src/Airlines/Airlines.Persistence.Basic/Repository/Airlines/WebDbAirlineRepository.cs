﻿using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Entities;
using AutoMapper;

namespace Airlines.Persistence.Basic.Repository.Airlines
{
    public class WebDbAirlineRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Airline, Airline>(dbContext, dbContext.Airlines, mapper),
            IAirlineRepository<Airline>
    {
        public override bool Contains(Airline item) => _dbSet.Contains(item);

        public override Airline? FindByKey(string key)
        {
            var id = int.Parse(key);

            return _dbSet.SingleOrDefault(x => x.Id == id);
        }
    }
}
