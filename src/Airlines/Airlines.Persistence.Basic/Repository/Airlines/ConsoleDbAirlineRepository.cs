﻿using Airlines.Persistence.Basic.Container;
using AutoMapper;

namespace Airlines.Persistence.Basic.Repository.Airlines
{
    public class ConsoleDbAirlineRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Entities.Airline, Models.Airline>(dbContext, dbContext.Airlines, mapper),
            IAirlineRepository<Models.Airline>
    {
        public override bool Contains(Models.Airline item) => _dbSet.Any(x => x.Name == item.Name);

        public override Models.Airline? FindByKey(string key)
            => _mapper.Map<Models.Airline>(_dbSet.FirstOrDefault(x => x.Name == key));
    }
}
