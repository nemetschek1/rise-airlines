﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Repository
{
    public abstract class DbRepository<TEntity, TModel>(
        DbContext dbContext, DbSet<TEntity> dataSet, IMapper mapper) : IRepository<TModel>
        where TEntity : class
    {
        private readonly DbContext _dbContext = dbContext;

        protected readonly IMapper _mapper = mapper;
        protected readonly DbSet<TEntity> _dbSet = dataSet;

        public virtual int Count => _dbSet.Count();

        public virtual bool Add(TModel item)
        {
            var itemEntity = _mapper.Map<TEntity>(item);

            _dbSet.Add(itemEntity);

            return SaveChanges();
        }

        public virtual IEnumerable<TModel> All() => _dbSet
            .Select(x => x)
            .ProjectTo<TModel>(_mapper.ConfigurationProvider);

        public abstract bool Contains(TModel item);

        public abstract TModel? FindByKey(string key);

        public IEnumerable<TModel> Sort(bool orderAscending = true)
        {
            var sortedCollection = orderAscending ? _dbSet.OrderBy(x => x) : _dbSet.OrderByDescending(x => x);

            return sortedCollection.ProjectTo<TModel>(_mapper.ConfigurationProvider);
        }

        public bool Update(TModel item)
        {
            var itemEntity = _mapper.Map<TEntity>(item);

            _dbSet.Update(itemEntity);

            return SaveChanges();
        }

        public bool Delete(string key)
        {
            var item = FindByKey(key);
            if (item == null)
            {
                return false;
            }

            var itemEntity = item is TEntity castedItem ? castedItem : _mapper.Map<TEntity>(item);

            _dbSet.Remove(itemEntity);

            return SaveChanges();
        }

        private bool SaveChanges()
            => _dbContext.SaveChanges() > 0;
    }
}
