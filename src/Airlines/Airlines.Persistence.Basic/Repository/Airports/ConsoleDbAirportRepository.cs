﻿using Airlines.Persistence.Basic.Container;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Airlines.Persistence.Basic.Repository.Airports
{
    public class ConsoleDbAirportRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Entities.Airport, Models.Airport>(dbContext, dbContext.Airports, mapper),
            IAirportRepository<Models.Airport>
    {
        public IEnumerable<Models.Airport> AllWithCityName(string cityName)
            => _dbSet.Where(x => x.City == cityName).ProjectTo<Models.Airport>(_mapper.ConfigurationProvider);

        public IEnumerable<Models.Airport> AllWithCountryName(string countryName)
            => _dbSet.Where(x => x.Country == countryName).ProjectTo<Models.Airport>(_mapper.ConfigurationProvider);

        public override bool Contains(Models.Airport item) => _dbSet.Any(x => x.Name == item.Name);

        public override Models.Airport? FindByKey(string key)
            => _mapper.Map<Models.Airport>(_dbSet.FirstOrDefault(x => x.Name == key));
    }
}
