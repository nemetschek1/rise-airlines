﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Container;

namespace Airlines.Persistence.Basic.Repository.Airports
{
    public class WebDbAirportRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Airport, Airport>(dbContext, dbContext.Airports, mapper),
            IAirportRepository<Airport>
    {
        public IEnumerable<Airport> AllWithCityName(string cityName)
            => _dbSet.Where(x => x.City == cityName);

        public IEnumerable<Airport> AllWithCountryName(string countryName)
            => _dbSet.Where(x => x.Country == countryName);

        public override bool Contains(Airport item) => _dbSet.Contains(item);

        public override Airport? FindByKey(string key)
            => _dbSet.SingleOrDefault(x => x.Code == key);
    }
}
