﻿namespace Airlines.Persistence.Basic.Repository.Airports
{
    public interface IAirportRepository<T> : IRepository<T>
    {
        IEnumerable<T> AllWithCityName(string cityName);

        IEnumerable<T> AllWithCountryName(string countryName);
    }
}
