﻿using Airlines.Persistence.Basic.Algorithms.Sort;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.Airports
{
    public class InMemoryAirportRepository : InMemoryRepository<Airport>, IAirportRepository<Airport>
    {
        private static readonly SortAlgorithm _sortingAlgorithm = new BubbleSort();

        private readonly Dictionary<string, List<Airport>> _airportsWithCitiesNames = [];
        private readonly Dictionary<string, List<Airport>> _airportsWithCountriesNames = [];

        public override bool Add(Airport item)
        {
            if (_collection.Contains(item))
            {
                return false;
            }

            base.Add(item);

            if (item.Country != null)
            {
                if (!_airportsWithCountriesNames.TryGetValue(item.Country, out var list))
                {
                    list = [];
                    _airportsWithCountriesNames.Add(item.Country, list);
                }

                list.Add(item);
            }

            if (item.City != null)
            {
                if (!_airportsWithCitiesNames.TryGetValue(item.City, out var list))
                {
                    list = [];
                    _airportsWithCitiesNames.Add(item.City, list);
                }

                list.Add(item);
            }

            return true;
        }

        public IEnumerable<Airport> AllWithCityName(string cityName)
            => _airportsWithCitiesNames.TryGetValue(cityName, out var value) ? value : [];

        public IEnumerable<Airport> AllWithCountryName(string countryName)
            => _airportsWithCountriesNames.TryGetValue(countryName, out var value) ? value : [];

        public override IEnumerable<Airport> Sort(bool orderAscending = true)
            => _sortingAlgorithm.Execute(All(), orderAscending);
    }
}
