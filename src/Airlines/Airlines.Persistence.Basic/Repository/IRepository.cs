﻿namespace Airlines.Persistence.Basic.Repository
{
    public interface IRepository<T>
    {
        int Count { get; }

        bool Add(T item);

        IEnumerable<T> All();

        T? FindByKey(string key);

        bool Contains(T item);

        IEnumerable<T> Sort(bool orderAscending = true);

        bool Update(T item);

        bool Delete(string key);
    }
}
