﻿using Airlines.Persistence.Basic.Models.Aircrafts;

namespace Airlines.Persistence.Basic.Repository.Aircrafts
{
    public class InMemoryAircraftRepository : InMemoryRepository<Aircraft>, IAircraftRepository<Aircraft>
    {
        public override IEnumerable<Aircraft> Sort(bool orderAscending = true)
            => throw new NotImplementedException();
    }
}
