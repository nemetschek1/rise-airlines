﻿using Airlines.Persistence.Basic.Algorithms.Sort;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.Flights
{
    public class InMemoryFlightRepository : InMemoryRepository<Flight>, IFlightRepository<Flight>
    {
        private static readonly SortAlgorithm _sortingAlgorithm = new SelectionSort();

        public override bool Add(Flight item)
        {
            base.Add(item);

            item.DepartureAirport!.FlightsFromCurrent.Add(item);

            return true;
        }

        public override IEnumerable<Flight> Sort(bool orderAscending = true)
            => _sortingAlgorithm.Execute(All(), orderAscending);
    }
}