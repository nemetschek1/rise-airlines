﻿using AutoMapper;
using Airlines.Persistence.Basic.Container;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repository.Flights
{
    public class WebDbFlightRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Flight, Flight>(dbContext, dbContext.Flights, mapper),
            IFlightRepository<Flight>
    {
        public override bool Contains(Flight item) => _dbSet.Contains(item);

        public override Flight? FindByKey(string key)
        {
            var id = int.Parse(key);

            return _dbSet.SingleOrDefault(x => x.Id == id);
        }
    }
}
