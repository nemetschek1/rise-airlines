﻿using AutoMapper;
using Airlines.Persistence.Basic.Container;

namespace Airlines.Persistence.Basic.Repository.Flights
{
    public class ConsoleDbFlightRepository(ApplicationDbContext dbContext, IMapper mapper)
        : DbRepository<Entities.Flight, Models.Flight>(dbContext, dbContext.Flights, mapper),
            IFlightRepository<Models.Flight>
    {
        public override bool Contains(Models.Flight item) => _dbSet.Any(x => x.Number == item.Identifier);

        public override Models.Flight? FindByKey(string key)
            => _mapper.Map<Models.Flight>(_dbSet.FirstOrDefault(x => x.Number == key));
    }
}
