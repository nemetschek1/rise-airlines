﻿using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Models.Aircrafts;
using Airlines.Persistence.Basic.Repository.Aircrafts;
using Airlines.Persistence.Basic.Repository.Airlines;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;
using Airlines.Persistence.Basic.Repository.RouteFlights;

namespace Airlines.Persistence.Basic.Container
{
    public class RepositoryContainer
    {
        public RepositoryContainer()
        {
        }

        public RepositoryContainer(IFlightRepository<Flight> flights,
            IAirportRepository<Airport> airports,
            IAircraftRepository<Aircraft> aircrafts,
            IAirlineRepository<Airline> airlines,
            IRouteFlightRepository<Flight> routeFlights)
        {
            Flights = flights;
            Airports = airports;
            Aircrafts = aircrafts;
            Airlines = airlines;
            RouteFlights = routeFlights;
        }

        public IFlightRepository<Flight> Flights { get; init; }

        public IAirportRepository<Airport> Airports { get; init; }

        public IAircraftRepository<Aircraft> Aircrafts { get; init; }

        public IAirlineRepository<Airline> Airlines { get; init; }

        public IRouteFlightRepository<Flight> RouteFlights { get; init; }
    }
}
