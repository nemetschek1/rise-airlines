﻿using Airlines.Persistence.Basic.Entities;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Container
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Airline> Airlines { get; set; }

        public virtual DbSet<Airport> Airports { get; set; }

        public virtual DbSet<Flight> Flights { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Airline>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK__Airlines__3214EC074E9845B1");

                entity.Property(e => e.Description)
                    .HasMaxLength(256)
                    .IsUnicode(false);
                entity.Property(e => e.Name)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Airport>(entity =>
            {
                entity.HasKey(e => e.Code).HasName("PK__Airports__A25C5AA6D55E2437");

                entity.HasIndex(e => e.City, "IX_Airports_City");

                entity.HasIndex(e => e.Country, "IX_Airports_Country");

                entity.Property(e => e.Code)
                    .HasMaxLength(4)
                    .IsUnicode(false);
                entity.Property(e => e.City)
                    .HasMaxLength(256)
                    .IsUnicode(false);
                entity.Property(e => e.Country)
                    .HasMaxLength(256)
                    .IsUnicode(false);
                entity.Property(e => e.Name)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Flight>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK__Flights__3214EC074381202C");

                entity.Property(e => e.Arrival).HasColumnType("datetime");
                entity.Property(e => e.Departure).HasColumnType("datetime");
                entity.Property(e => e.From)
                    .HasMaxLength(4)
                    .IsUnicode(false);
                entity.Property(e => e.Number)
                    .HasMaxLength(256)
                    .IsUnicode(false);
                entity.Property(e => e.To)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.FromNavigation).WithMany(p => p.FlightFromNavigations)
                    .HasForeignKey(d => d.From)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Flights__From__5BE2A6F2");

                entity.HasOne(d => d.ToNavigation).WithMany(p => p.FlightToNavigations)
                    .HasForeignKey(d => d.To)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Flights__To__5CD6CB2B");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}