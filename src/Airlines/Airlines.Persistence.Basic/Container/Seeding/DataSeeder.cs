﻿using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Models.Aircrafts;
using Airlines.Persistence.Basic.Repository;

namespace Airlines.Persistence.Basic.Container.Seeding
{
    public class DataSeeder
    {
        public static void SeedInMemoryRepositoryContainer(RepositoryContainer repositoryContainer)
        {
            SeedAircrafts(repositoryContainer);
            SeedAirports(repositoryContainer);
            SeedAirlines(repositoryContainer);
            SeedFlights(repositoryContainer);
            SeedRouteFlights(repositoryContainer);
        }

        public static void SeedDbRepositoryContainer(RepositoryContainer repositoryContainer)
        {
            SeedAircrafts(repositoryContainer);
            SeedRouteFlights(repositoryContainer);
        }

        private static void SeedAircrafts(RepositoryContainer repositoryContainer,
            string path = "./Resources/aircrafts.csv",
            BusinessDomainObjectsTypes type = BusinessDomainObjectsTypes.Aircraft)
        {
            var aircrafts = BusinessDomainObjectsReader.Get<Aircraft>(repositoryContainer, type, path);

            SeedItems(repositoryContainer.Aircrafts, aircrafts);
        }

        private static void SeedAirports(RepositoryContainer repositoryContainer,
            string path = "./Resources/airports.csv",
            BusinessDomainObjectsTypes type = BusinessDomainObjectsTypes.Airport)
        {
            var airports = BusinessDomainObjectsReader.Get<Airport>(repositoryContainer, type, path);

            SeedItems(repositoryContainer.Airports, airports);
        }

        private static void SeedAirlines(RepositoryContainer repositoryContainer,
            string path = "./Resources/airlines.csv",
            BusinessDomainObjectsTypes type = BusinessDomainObjectsTypes.Airline)
        {
            var airlines = BusinessDomainObjectsReader.Get<Airline>(repositoryContainer, type, path);

            SeedItems(repositoryContainer.Airlines, airlines);
        }

        private static void SeedFlights(RepositoryContainer repositoryContainer,
            string path = "./Resources/flights.csv",
            BusinessDomainObjectsTypes type = BusinessDomainObjectsTypes.Flight)
        {
            var flights = BusinessDomainObjectsReader.Get<Flight>(repositoryContainer, type, path);

            SeedItems(repositoryContainer.Flights, flights);
        }

        private static void SeedRouteFlights(RepositoryContainer repositoryContainer,
           string path = "./Resources/route.csv",
           BusinessDomainObjectsTypes type = BusinessDomainObjectsTypes.Route)
        {
            var routeFlights = BusinessDomainObjectsReader.Get<Flight>(repositoryContainer, type, path);

            SeedItems(repositoryContainer.RouteFlights, routeFlights);
        }

        private static void SeedItems<T>(IRepository<T> repository, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                repository.Add(item);
            }
        }
    }
}