﻿namespace Airlines.Persistence.Basic.Container.Seeding
{
    public static class FileReader
    {
        public static IEnumerable<string[]> ReadBusinessDomainObjects(
            string path, string lineElementsSplitter = ", ")
        {
            if (!File.Exists(path))
                return [];

            var fileAllLines = new List<string[]>();

            using (var sr = new StreamReader(path))
            {
                var line = sr.ReadLine();

                while ((line = sr.ReadLine()) != null)
                    fileAllLines.Add(line?.Split(lineElementsSplitter) ?? []);
            }

            return fileAllLines;
        }
    }
}
