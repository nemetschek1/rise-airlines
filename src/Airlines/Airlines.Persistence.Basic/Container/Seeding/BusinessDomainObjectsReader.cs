﻿using Airlines.DataContracts.Enums;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Container.Seeding
{
    public class BusinessDomainObjectsReader
    {
        public static ICollection<T> Get<T>(
            RepositoryContainer repositoryContainer,
            BusinessDomainObjectsTypes businessDomainObjectType,
            string filePath)
            where T : BusinessDomainObject
        {
            var newItemsValues = FileReader.ReadBusinessDomainObjects(filePath);
            var collection = new List<T>();

            foreach (var itemData in newItemsValues)
            {
                try
                {
                    var newItem = CreateNewItem(repositoryContainer, businessDomainObjectType, itemData);

                    collection.Add((T)newItem);
                }
                catch
                {
                    // Could log error
                    // For now there is no clear business requirement what to do
                }
            }

            return collection;
        }

        private static BusinessDomainObject CreateNewItem(
            RepositoryContainer repositoryContainer,
            BusinessDomainObjectsTypes type,
            string[] newItemValues)
        {
            return type switch
            {
                BusinessDomainObjectsTypes.Airport => BusinessDomainObjectsFactory
                    .CreateAirport(newItemValues[0], newItemValues[1], newItemValues[2], newItemValues[3]),
                BusinessDomainObjectsTypes.Airline => BusinessDomainObjectsFactory
                    .CreateAirline(newItemValues[0]),
                BusinessDomainObjectsTypes.Flight => BusinessDomainObjectsFactory
                    .CreateFlight(newItemValues[0], newItemValues[1], newItemValues[2], newItemValues[3], newItemValues[4], newItemValues[5], repositoryContainer.Airports, repositoryContainer.Aircrafts),
                BusinessDomainObjectsTypes.Route => BusinessDomainObjectsFactory
                    .CreateRouteFlight(newItemValues[0], repositoryContainer.Flights),
                BusinessDomainObjectsTypes.Aircraft => BusinessDomainObjectsFactory
                    .CreateAircraft(newItemValues[0], newItemValues[1], newItemValues[2], newItemValues[3]),
                _ => throw new NotImplementedException(),
            };
        }
    }
}
