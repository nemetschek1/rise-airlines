﻿using Airlines.DataContracts.Enums;

namespace Airlines.Persistence.Basic.Models
{
    public abstract class BusinessDomainObject : IComparable<BusinessDomainObject>
    {
        public int CompareTo(BusinessDomainObject? other)
        {
            if (other == null)
            {
                return (int)ComparingTypes.Bigger;
            }

            var currentComparingValue = GetComparingValue();

            if (currentComparingValue == null)
            {
                return (int)ComparingTypes.Smaller;
            }

            var otherComparingValue = other.GetComparingValue();

            if (otherComparingValue == null)
            {
                return (int)ComparingTypes.Bigger;
            }

            return currentComparingValue.CompareTo(otherComparingValue);
        }

        internal abstract string? GetComparingValue();

        public override int GetHashCode()
            => (GetComparingValue() ?? string.Empty).GetHashCode();

        public override bool Equals(object? obj)
            => obj?.GetHashCode() == GetHashCode();
    }
}
