﻿using Airlines.Persistence.Basic.Models.Aircrafts;

namespace Airlines.Persistence.Basic.Models
{
    public class Flight(string identifier) : BusinessDomainObject
    {
        public Flight(string identifier,
            Airport departureAirport,
            Airport arrivalAirport,
            Aircraft aircraft,
            decimal price,
            double durationInHours)
            : this(identifier)
        {
            DepartureAirport = departureAirport;
            ArrivalAirport = arrivalAirport;
            Aircraft = aircraft;
            Price = price;
            DurationInHours = durationInHours;
        }

        public string Identifier { get; set; } = identifier;

        public Airport? DepartureAirport { get; set; }

        public Airport? ArrivalAirport { get; set; }

        public Aircraft? Aircraft { get; set; }

        public decimal? Price { get; set; }

        public double? DurationInHours { get; set; }

        internal override string? GetComparingValue() => Identifier;

        public override string ToString() => $"Identifier: {Identifier}, Departure: {DepartureAirport} -> Arrival: {ArrivalAirport}";
    }
}
