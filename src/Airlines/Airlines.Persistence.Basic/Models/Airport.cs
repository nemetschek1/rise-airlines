﻿using System.Diagnostics.CodeAnalysis;

namespace Airlines.Persistence.Basic.Models
{
    public class Airport(string name) : BusinessDomainObject
    {
        [SetsRequiredMembers]
        public Airport(string identifier, string name, string city, string country)
            : this(name)
        {
            Identifier = identifier;
            City = city;
            Country = country;
        }

        public string? Identifier { get; set; }

        public string Name { get; set; } = name;

        public string? City { get; set; }

        public string? Country { get; set; }

        public HashSet<Flight> FlightsFromCurrent { get; set; } = [];

        internal override string? GetComparingValue() => Name;

        public override string ToString()
            => $"Identifier: {Identifier}, Name: {Name}, City: {City}, Country: {Country}";
    }
}
