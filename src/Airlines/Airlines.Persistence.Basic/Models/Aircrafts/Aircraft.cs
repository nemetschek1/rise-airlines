﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public abstract class Aircraft(string model) : BusinessDomainObject
    {
        public string Model { get; set; } = model;

        internal override string? GetComparingValue() => Model;
    }
}
