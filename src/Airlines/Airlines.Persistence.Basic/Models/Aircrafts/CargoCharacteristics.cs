﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public class CargoCharacteristics
    {
        private double _weight;
        private double _volume;

        public CargoCharacteristics(double weight, double volume)
        {
            Weight = weight;
            Volume = volume;
        }

        public double Weight
        {
            get => _weight;
            set
            {
                if (_weight < 0)
                {
                    throw new ArgumentException($"{nameof(Weight)} can't be less than 0!");
                }

                _weight = value;
            }
        }

        public double Volume
        {
            get => _volume;
            set
            {
                if (_volume < 0)
                {
                    throw new ArgumentException($"{nameof(Volume)} can't be less than 0!");
                }

                _volume = value;
            }
        }
    }
}