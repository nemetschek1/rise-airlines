﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public class AircraftFactory
    {
        public static Aircraft CreateAircraft(
            string? model, double? cargoWeight, double? cargoVolume, uint? seats)
        {
            if (seats == null)
            {
                return new CargoAircraft(model!, cargoWeight!.Value, cargoVolume!.Value);
            }
            else if (cargoWeight == null)
            {
                return new PrivateAircraft(model!, seats!.Value);
            }
            else
            {
                return new PassengerAircraft(model!, seats!.Value, cargoWeight!.Value, cargoVolume!.Value);
            }
        }
    }
}
