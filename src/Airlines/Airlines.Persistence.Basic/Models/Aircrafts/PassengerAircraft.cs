﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public class PassengerAircraft(string model, uint seats, double cargoWeight, double cargoVolume)
        : Aircraft(model)
    {
        public CargoCharacteristics Cargo { get; } = new CargoCharacteristics(cargoWeight, cargoVolume);

        public uint Seats { get; set; } = seats;
    }
}
