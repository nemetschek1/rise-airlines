﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public class CargoAircraft(string model, double cargoWeight, double cargoVolume)
        : Aircraft(model)
    {
        public CargoCharacteristics Cargo { get; } = new CargoCharacteristics(cargoWeight, cargoVolume);
    }
}
