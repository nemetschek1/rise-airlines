﻿namespace Airlines.Persistence.Basic.Models.Aircrafts
{
    public class PrivateAircraft(string model, uint seats) : Aircraft(model)
    {
        public uint Seats { get; set; } = seats;
    }
}
