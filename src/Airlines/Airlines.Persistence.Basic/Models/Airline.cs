﻿namespace Airlines.Persistence.Basic.Models
{
    public class Airline(string name) : BusinessDomainObject
    {
        public string Name { get; set; } = name;

        internal override string? GetComparingValue() => Name;

        public override string ToString() => Name ?? string.Empty;
    }
}
