﻿namespace Airlines.Persistence.Basic.Models
{
    public class SearchTerm(string text) : BusinessDomainObject
    {
        public string Text { get; set; } = text;

        internal override string GetComparingValue() => Text;
    }
}
