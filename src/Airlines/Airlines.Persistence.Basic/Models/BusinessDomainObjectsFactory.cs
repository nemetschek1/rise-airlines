﻿using Airlines.Persistence.Basic.Models.Aircrafts;
using Airlines.Persistence.Basic.Repository.Aircrafts;
using System.Globalization;
using Airlines.Persistence.Basic.Validation;
using Airlines.Persistence.Basic.Repository.Airports;
using Airlines.Persistence.Basic.Repository.Flights;

namespace Airlines.Persistence.Basic.Models
{
    public static class BusinessDomainObjectsFactory
    {
        public static Airport CreateAirport(string identifier, string name, string city, string country)
        {
            var airport = new Airport(identifier, name, city, country);

            AirportValidationHandler.Validate(airport);

            return airport;
        }

        public static Airline CreateAirline(string name)
        {
            var airline = new Airline(name);

            AirlineValidationHandler.Validate(airline);

            return airline;
        }

        public static Flight CreateFlight(
            string flightIdentifier,
            string departureAirportName,
            string arrivalAirportName,
            string aircraftModel,
            string priceText,
            string durationInHoursText,
            IAirportRepository<Airport> airportRepository,
            IAircraftRepository<Aircraft> aircraftRepository)
        {
            var departureAirport = airportRepository.FindByKey(departureAirportName);
            ArgumentNullException.ThrowIfNull(departureAirport);

            var arrivalAirport = airportRepository.FindByKey(arrivalAirportName);
            ArgumentNullException.ThrowIfNull(arrivalAirport);

            var aircraft = aircraftRepository.FindByKey(aircraftModel);
            ArgumentNullException.ThrowIfNull(aircraft);

            var price = decimal.Parse(priceText, CultureInfo.InvariantCulture);
            var durationInHours = double.Parse(durationInHoursText, CultureInfo.InvariantCulture);

            var filght = new Flight(
                flightIdentifier, departureAirport, arrivalAirport, aircraft, price, durationInHours);

            FlightValidationHandler.Validate(filght);

            return filght;
        }

        public static Aircraft CreateAircraft(string model, string cargoWeightText, string cargoVolumeText, string seatsText)
        {
            double? cargoWeight = null;
            if (IsAircraftInputParameterValid(cargoWeightText))
                cargoWeight = double.Parse(cargoWeightText, CultureInfo.InvariantCulture);

            double? cargoVolume = null;
            if (IsAircraftInputParameterValid(cargoVolumeText))
                cargoVolume = double.Parse(cargoVolumeText, CultureInfo.InvariantCulture);

            uint? seats = null;
            if (IsAircraftInputParameterValid(seatsText))
                seats = uint.Parse(seatsText);

            var aircraft = AircraftFactory.CreateAircraft(model, cargoWeight, cargoVolume, seats);

            AircraftValidationHandler.Validate(aircraft);

            return aircraft;
        }

        public static Flight CreateRouteFlight(string flightIdentifier, IFlightRepository<Flight> flightRepository)
        {
            var flight = flightRepository.FindByKey(flightIdentifier);

            RouteValidationHandler.Validate(flight);

            return flight!;
        }

        private static bool IsAircraftInputParameterValid(string input)
            => input != "-";
    }
}
