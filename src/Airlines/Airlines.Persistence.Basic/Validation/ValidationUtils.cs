﻿namespace Airlines.Persistence.Basic.Validation
{
    internal static class ValidationUtils
    {
        internal static bool CheckIfAllCharactersAreAlphanumeric(this string text)
        {
            foreach (var character in text)
            {
                if (!(char.IsAsciiDigit(character) || char.IsAsciiLetter(character)))
                {
                    return false;
                }
            }

            return true;
        }

        internal static bool CheckIfAllCharactersAreAlphabetic(this string text)
        {
            foreach (var letter in text)
            {
                if (!char.IsAsciiLetter(letter))
                {
                    return false;
                }
            }

            return true;
        }

        internal static bool CheckIfAllCharactersAreAlphabeticAndSpaces(this string text)
        {
            foreach (var letter in text)
            {
                if (!(char.IsAsciiLetter(letter) || letter == ' '))
                {
                    return false;
                }
            }

            return true;
        }

        internal static void ValidateTextContainsOnlyAlphabeticAndSpaceCharacters(this string? text, string checkedValueName)
        {
            if (text != null)
            {
                text = text.Trim();
            }

            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException($"'{checkedValueName}' is incorrect! It should NOT be empty!");
            }

            if (!text.CheckIfAllCharactersAreAlphabeticAndSpaces())
            {
                throw new ArgumentException($"'{checkedValueName}' does NOT consist of only alphabetic letters and spaces!");
            }
        }
    }
}
