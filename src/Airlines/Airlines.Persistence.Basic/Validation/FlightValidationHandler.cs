﻿using Airlines.Persistence.Basic.Models;
using Airlines.DataContracts.Exceptions.Validation.Flight;

namespace Airlines.Persistence.Basic.Validation
{
    internal static class FlightValidationHandler
    {
        internal static void Validate(Flight flight)
        {
            ArgumentNullException.ThrowIfNull(flight.Aircraft);
            ArgumentNullException.ThrowIfNull(flight.DepartureAirport);
            ArgumentNullException.ThrowIfNull(flight.ArrivalAirport);

            ValidateIdentifier(flight.Identifier);
            ValidatePrice(flight.Price);
            ValidateDurationInHours(flight.DurationInHours);

            if (flight.DepartureAirport == flight.ArrivalAirport)
            {
                throw new InvalidFlightParametersException("DepartureAirport and ArrivalAirport can NOT be equal!");
            }
        }

        internal static void ValidateIdentifier(string? identifier)
        {
            if (identifier != null)
            {
                identifier = identifier.Trim();
            }

            if (string.IsNullOrEmpty(identifier))
            {
                throw new InvalidFlightParametersException("Identifier is required!");
            }

            if (!identifier.CheckIfAllCharactersAreAlphanumeric())
            {
                throw new InvalidFlightParametersException("Identifier should consist of only alphabetic letters!");
            }
        }

        private static void ValidatePrice(decimal? price)
        {
            ArgumentNullException.ThrowIfNull(price);

            if (price <= 0)
            {
                throw new InvalidFlightParametersException("Price needs to be positive!");
            }
        }

        private static void ValidateDurationInHours(double? durationInHours)
        {
            ArgumentNullException.ThrowIfNull(durationInHours);

            if (durationInHours <= 0)
            {
                throw new InvalidFlightParametersException("Duration needs to be positive!");
            }
        }
    }
}
