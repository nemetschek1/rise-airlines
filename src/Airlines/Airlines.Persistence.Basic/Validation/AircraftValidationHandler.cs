﻿using Airlines.Persistence.Basic.Models.Aircrafts;

namespace Airlines.Persistence.Basic.Validation
{
    public class AircraftValidationHandler
    {
        internal static void Validate(Aircraft aircraft)
            => ArgumentNullException.ThrowIfNull(aircraft.Model);
    }
}
