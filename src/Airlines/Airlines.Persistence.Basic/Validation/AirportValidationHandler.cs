﻿using Airlines.Persistence.Basic.Models;
using Airlines.DataContracts.Exceptions.Validation.Airport;

namespace Airlines.Persistence.Basic.Validation
{
    internal static class AirportValidationHandler
    {
        internal const int NAME_REQUIRED_LENGTH = 3;

        internal const int IDENTIFIER_MIN_LENGTH = 2;
        internal const int IDENTIFIER_MAX_LENGTH = 4;

        internal static void Validate(Airport airport)
        {
            ValidateIdentifier(airport.Identifier);
            ValidateName(airport.Name);
            ValidateCity(airport.City);
            ValidateCountry(airport.Country);
        }

        private static void ValidateIdentifier(string? identifier)
        {
            ArgumentNullException.ThrowIfNull(identifier);

            identifier = identifier.Trim();

            if (identifier.Length is not (>= IDENTIFIER_MIN_LENGTH and <= IDENTIFIER_MAX_LENGTH))
            {
                throw new InvalidAirportIdentifierException("Should be between 2 and 4!");
            }

            if (!identifier.CheckIfAllCharactersAreAlphanumeric())
            {
                throw new InvalidAirportIdentifierException("Should consist of only alphabetic letters!");
            }
        }

        internal static void ValidateName(string? name)
            => ValidationUtils.ValidateTextContainsOnlyAlphabeticAndSpaceCharacters(name, nameof(name));

        private static void ValidateCity(string? city)
            => ValidationUtils.ValidateTextContainsOnlyAlphabeticAndSpaceCharacters(city, nameof(city));

        private static void ValidateCountry(string? country)
            => ValidationUtils.ValidateTextContainsOnlyAlphabeticAndSpaceCharacters(country, nameof(country));
    }
}
