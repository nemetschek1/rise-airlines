﻿using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Validation
{
    public class RouteValidationHandler
    {
        public static void Validate(Flight? newFlight)
        {
            ArgumentNullException.ThrowIfNull(newFlight);

            ValidateNewFlight(newFlight);
        }

        private static void ValidateNewFlight(Flight? flight)
            => FlightValidationHandler.ValidateIdentifier(flight?.Identifier);
    }
}
