﻿using Airlines.Persistence.Basic.Models;
using Airlines.DataContracts.Exceptions.Validation.Airline;

namespace Airlines.Persistence.Basic.Validation
{
    internal static class AirlineValidationHandler
    {
        internal const int NAME_MAX_LENGTH = 6;

        internal static void Validate(Airline airline) => ValidateName(airline.Name);

        private static void ValidateName(string? name)
        {
            ArgumentNullException.ThrowIfNull(name);

            name = name.Trim();

            if (string.IsNullOrEmpty(name))
            {
                throw new InvalidAirlineNameException("It should NOT be empty!");
            }

            if (!(name.Length < NAME_MAX_LENGTH))
            {
                throw new InvalidAirlineNameException($"Expected length: less than {NAME_MAX_LENGTH}. Actual length: {name.Length}");
            }
        }
    }
}
