﻿using Airlines.DataContracts.Enums;

namespace Airlines.Persistence.Basic.Algorithms.Sort
{
    public abstract class SortAlgorithm
    {
        public abstract IEnumerable<T> Execute<T>(IEnumerable<T> collection, bool orderAscending = true)
            where T : IComparable<T>;

        protected static bool Compare<T>(T origin, T comparedTo, bool orderAscending)
            where T : IComparable<T>
        {
            if (orderAscending)
            {
                return origin.CompareTo(comparedTo) == (int)ComparingTypes.Smaller;
            }

            return origin.CompareTo(comparedTo) == (int)ComparingTypes.Bigger;
        }

        protected static void SwapItems<T>(IList<T> array, int indexItemOne, int indexItemTwo)
            => (array[indexItemOne], array[indexItemTwo]) = (array[indexItemTwo], array[indexItemOne]);
    }
}
