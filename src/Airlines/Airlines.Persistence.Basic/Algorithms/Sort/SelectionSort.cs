﻿namespace Airlines.Persistence.Basic.Algorithms.Sort
{
    public class SelectionSort : SortAlgorithm
    {
        public override IEnumerable<T> Execute<T>(IEnumerable<T> collection, bool orderAscending = true)
        {
            var array = collection.ToArray();
            var arrayLength = array.Length;
            for (var i = 0; i < arrayLength - 1; i++)
            {
                var smallestVal = i;
                for (var j = i + 1; j < arrayLength; j++)
                {
                    if (Compare(array[j], array[smallestVal], orderAscending))
                    {
                        smallestVal = j;
                    }
                }

                SwapItems(array, smallestVal, i);
            }

            return array;
        }
    }
}
