﻿namespace Airlines.Persistence.Basic.Algorithms.Sort
{
    public class BubbleSort : SortAlgorithm
    {
        public override IEnumerable<T> Execute<T>(IEnumerable<T> collection, bool orderAscending = true)
        {
            var array = collection.ToArray();

            var isSorted = true;
            var lastIndex = array.Length - 1;

            for (var i = 1; (i <= lastIndex) && isSorted; i++)
            {
                isSorted = false;
                for (var j = 0; j < lastIndex; j++)
                {
                    if (Compare(array[j + 1], array[j], orderAscending))
                    {
                        SwapItems(array, j, j + 1);
                        isSorted = true;
                    }
                }
            }

            return array;
        }
    }
}