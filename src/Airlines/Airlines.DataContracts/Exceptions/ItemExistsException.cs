﻿namespace Airlines.DataContracts.Exceptions
{
    public class ItemExistsException(string type) : ArgumentException($"Item of type '{type}' already exists!")
    {
    }
}
