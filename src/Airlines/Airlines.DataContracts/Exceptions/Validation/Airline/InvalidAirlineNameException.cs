﻿namespace Airlines.DataContracts.Exceptions.Validation.Airline
{
    public class InvalidAirlineNameException(string message)
        : ArgumentException($"${nameof(Airline)} 'Name' input is invalid! ${message}")
    {
    }
}
