﻿namespace Airlines.DataContracts.Exceptions.Validation.Route
{
    public class FindRouteNodeParentException(string message)
            : ArgumentException($"Can't find route node parent, because {message}")
    {
    }
}
