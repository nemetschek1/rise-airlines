﻿namespace Airlines.DataContracts.Exceptions.Validation.Route
{
    public class InvalidRouteParametersException(string message)
        : ArgumentException($"Route input is invalid! {message}")
    {
    }
}
