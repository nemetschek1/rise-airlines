﻿namespace Airlines.DataContracts.Exceptions.Validation.Aircraft
{
    public class InvalidAircraftTypeParametersException(string type, string propertyName)
        : ArgumentException($"For {nameof(Aircraft)} of type {type}, you need {propertyName}")
    {
    }
}
