﻿namespace Airlines.DataContracts.Exceptions.Validation.Aircraft
{
    public class InvalidAircraftMethodException() : SystemException("Aicraft type doesn't support this method!")
    {
    }
}
