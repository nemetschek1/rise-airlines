﻿namespace Airlines.DataContracts.Exceptions.Validation.Flight
{
    public class InvalidFlightParametersException(string message)
        : ArgumentException($"Flight input is invalid! {message}")
    {
    }
}
