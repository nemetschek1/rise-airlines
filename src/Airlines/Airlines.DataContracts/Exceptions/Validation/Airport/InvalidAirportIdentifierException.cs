﻿namespace Airlines.DataContracts.Exceptions.Validation.Airport
{
    public class InvalidAirportIdentifierException(string message)
        : ArgumentException($"${nameof(Airport)} 'Identifier' input is invalid! ${message}")
    {
    }
}
