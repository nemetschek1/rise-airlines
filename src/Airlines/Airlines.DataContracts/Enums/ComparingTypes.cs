﻿namespace Airlines.DataContracts.Enums
{
    public enum ComparingTypes
    {
        Smaller = -1,
        Equal = 0,
        Bigger = 1,
    }
}
