﻿namespace Airlines.DataContracts.Enums
{
    public enum CommandsTypes
    {
        Search,
        Sort,
        Exists,
        List,
        RouteNew,
        RouteAdd,
        RouteRemove,
        RoutePrint,
        RouteFind,
        RouteCheck,
        RouteSearch,
        ReserveCargo,
        ReserveTicket,
        BatchStart,
        BatchRun,
        BatchCancel,
    }
}
