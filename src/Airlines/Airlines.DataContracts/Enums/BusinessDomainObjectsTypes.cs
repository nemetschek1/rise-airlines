﻿namespace Airlines.DataContracts.Enums
{
    public enum BusinessDomainObjectsTypes
    {
        Airport,
        Airline,
        Flight,
        Route,
        Aircraft
    }
}
