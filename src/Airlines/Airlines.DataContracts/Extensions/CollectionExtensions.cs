﻿namespace Airlines.DataContracts.Extensions
{
    public static class CollectionExtensions
    {
        public static IEnumerable<T> TakeInRange<T>(this T[] array, int startIndex, int endIndex)
        {
            var result = new T[endIndex - startIndex + 1];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = array[startIndex + i];
            }

            return result;
        }
    }
}
