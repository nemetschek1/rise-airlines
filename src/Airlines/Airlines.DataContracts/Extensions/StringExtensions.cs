﻿namespace Airlines.DataContracts.Extensions
{
    public static class StringExtensions
    {
        public static T? ParseToEnum<T>(this string type)
            where T : struct, Enum
        {
            if (Enum.TryParse(type, true, out T parsedType))
            {
                return parsedType;
            }

            return null;
        }
    }
}
